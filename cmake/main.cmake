set (EFIT_INCLUDES
        ${EFIT_INCLUDES}
        "${PROJECT_SOURCE_DIR}/orbits/include"
        "${PROJECT_SOURCE_DIR}/utils/include"
)

set (EFIT_OBJECTS
        ${EFIT_OBJECTS}
        $<TARGET_OBJECTS:orbits_lib>
        $<TARGET_OBJECTS:utils_lib>
)

add_subdirectory(orbits)
add_subdirectory(utils)
add_subdirectory(main)
