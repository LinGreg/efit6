import re
import os
import yaml
import shutil
import itertools
import datetime
import sys
import getopt

def listdir_nohidden(path):
    for f in os.listdir(path):
        if not f.startswith('.'):
            yield f

# Check whether or not two files differ in their contents except for the date line
def same(f1,f2):
    file1 = open(f1,"r")
    file2 = open(f2,"r")
    for l1,l2 in itertools.izip_longest(file1,file2,fillvalue=''): 
        if l1 != l2:
              l1nospace = ''.join(l1.split()).lower() #remove spaces and make lowercase
              #print l1
              #print l2
              #print l1nospace
              if      not l1nospace.startswith("#\date") \
                  and not l1nospace.startswith("#\\date") \
                  and not l1nospace.startswith("//\date") \
                  and not l1nospace.startswith("//\\date") \
                  and not l1nospace.startswith("///\date") \
                  and not l1nospace.startswith("///\\date"): 
                 #print "Doesn't match!", file1, file2
                 #quit()
                 return False
    return True

# Compare a candidate file to an existing file, replacing only if they differ.
def update_only_if_different(existing, candidate):
    if not os.path.isfile(existing):
         os.rename(candidate,existing)
         print "\033[1;33m   Created "+re.sub("\\.\\/","",existing)+"\033[0m"
    elif same(existing, candidate): 
         os.remove(candidate)
         print "\033[1;33m   Existing "+re.sub("\\.\\/","",existing)+" is identical to candidate; leaving it untouched\033[0m"
    else:
         os.rename(candidate,existing)
         print "\033[1;33m   Updated "+re.sub("\\.\\/","",existing)+"\033[0m"

# Remove C/C++ comments from 'text' (From http://stackoverflow.com/questions/241327/python-snippet-to-remove-c-and-c-comments)
def comment_remover(text):
    def replacer(match):
        s = match.group(0)
        if s.startswith('/'):
            return ""
        else:
            return s
    pattern = re.compile(
        r'//.*?$|/\*.*?\*/|\'(?:\\.|[^\\\'])*\'|"(?:\\.|[^\\"])*"',
        re.DOTALL | re.MULTILINE
    )
    return re.sub(pattern, replacer, text[:])

# No empties from re.split
def neatsplit(regex,string):
    return [x for x in re.split(regex,string) if x != '']

def make_type(node):
    output = ""
    if type(node) is dict:
        for key, n in node.iteritems():
            output += make_type(n) + " " + key + "; "
    elif type(node) is list:
        output += "struct {"
        for l in node:
            output += make_type(l)
        output += "}"
    else:
        output = node
        
    return output

# Actual updater program
def main(argv):
    
    directories = []
    observables = {}
    interfaces = {}
    yaml_file = yaml.load(open("config.yaml"))
    #std_input  = "std::unordered_map<std::string, double> &, std::unordered_map<std::string, double> &"
    #std_vec = "std::vector"
    std_input  = "map_type<std::string, double> &, map_type<std::string, double> &"
    std_vec = "vec_type"
    
    if type(yaml_file) is dict:
        if "models" in yaml_file and type(yaml_file["models"]) is dict:
            for mod_name, models in yaml_file["models"].iteritems():
                if type(models) is dict:
                    directories.append(mod_name);
                    observables[mod_name] = []
                    interfaces[mod_name] = []
                    if "observables" in models and type(models["observables"]) is dict:
                        for key, obs in models["observables"].iteritems():
                            observables[mod_name].append("typedef " + make_type(obs) + " " + key + "_type;")
                    if "interfaces" in models and type(models["interfaces"]) is list:
                        for op in models["interfaces"]:
                            interfaces[mod_name].append("void operator()(" + std_input + "".join([", " + std_vec + "<" + inter + "_type> &" for inter in op if "observables" in models and inter in models["observables"]]) + ")")
                        
    for directory in directories:
        if not os.path.exists(directory):
            os.mkdir(directory)
        if not os.path.exists(directory + "/include"):
            os.mkdir(directory + "/include")
        if not os.path.exists(directory + "/src"):
            os.mkdir(directory + "/src")
            
        towrite = """
#ifndef __{1}_INTERFACE_HPP__
#define __{1}_INTERFACE_HPP__

#include <memory>
#include <utility>
#include \"efit_base.hpp\"
#include \"efit_ptr.hpp\"

namespace efit
{{
    
    namespace {0}
    {{
        
        class {0}_base : public efit::efit_base
        {{
        public:
{2}
{3}
            virtual ~{0}_base() {{}}
        }};
        
        typedef efit_ptr<{0}_base> {0}_ptr;
        
    }}
            
}}

#endif
""".format(directory, directory.upper(), "".join([" "*12 + obs + "\n" for obs in observables[directory]]), "".join([" "*12 + "virtual "+ inter + " = 0;\n" for inter in interfaces[directory]]))

        cmake = directory + "/include/" + directory + "_interface.hpp"
        candidate = cmake + ".candidate"
        with open(candidate,"w") as f: f.write(towrite)
        update_only_if_different(cmake, candidate)

        towrite = """    
#ifndef __{1}_HPP__
#define __{1}_HPP__

#define LOAD_{1}(tag, ...) REGISTER_ELEM({0}_factory, tag, __VA_ARGS__)

#include \"yaml-cpp/yaml.h\"
#include \"factory_registry.hpp\"
#include \"{0}_interface.hpp\"

namespace efit
{{
    
    namespace {0}
    {{
        
        registry
        {{
            typedef {0}_base * func_type(const YAML::Node &);
            efit::reg_elem<func_type> {0}_factory;
        }}
        
    }}
    
}}

#include \"{0}_rollcall.hpp\"

#endif
""".format(directory, directory.upper())
    
        cmake = directory + "/include/" + directory + ".hpp"
        candidate = cmake + ".candidate"
        with open(candidate,"w") as f: f.write(towrite)
        update_only_if_different(cmake, candidate)
    
    directories.append("utils");
    for directory in directories:
        if os.path.isdir(directory) and directory != "cmake" and directory != "build" and directory != "main" and directory != "bin":
            model_srcs = []
            model_hdrs = []
            model_hdrs_simple = []
            if directory != "utils":
                if os.path.exists(directory + "/src/models"):
                    model_srcs = [ root + "/" + f for root,dirs,files in os.walk(directory + "/src/models") for f in files if f.endswith(".cpp") or f.endswith(".c") or f.endswith(".cc") or f.endswith(".cxx") ]
                if os.path.exists(directory + "/include/models"):
                    model_hdrs = [ root + "/" + f for root,dirs,files in os.walk(directory + "/include/models") for f in files if f.endswith(".hpp") or f.endswith(".h") ]
                    model_hdrs_simple = [ f for f in listdir_nohidden(directory + "/include/models") if os.path.isfile(directory + "/include/models/" + f) and (f.endswith(".hpp") or f.endswith(".h")) ]

                models_txt_out = "#ifndef " + directory.upper() + "_ROLLCALL_HPP\n#define " + directory.upper() + "_ROLLCALL_HPP\n\n"
                
                for header in sorted(model_hdrs_simple):
                    models_txt_out += "#include \"models/" + header + "\"\n"
                
                models_txt_out += "\n#endif\n"
        
                towrite = models_txt_out

                header = directory + "/include/" + directory + "_rollcall.hpp"
                candidate = directory + "/include/" + directory + "_rollcall.hpp.candidate"
                with open(candidate,"w") as f: f.write(towrite)
                update_only_if_different(header, candidate)

            #Now entering cmake stuff.
            if not os.path.exists(directory + "/src"):
                os.mkdir(directory + "/src")
            if not os.path.exists(directory + "/include"):
                os.mkdir(directory + "/include")
            srcs = [ name for name in listdir_nohidden(directory + "/src") if os.path.isfile(directory + "/src/" + name) if name.endswith(".cpp") or name.endswith(".c") or name.endswith(".cc") or name.endswith(".cxx") ]
            hdrs = [ name for name in listdir_nohidden(directory + "/include") if os.path.isfile(directory + '/include/' + name) if name.endswith(".hpp") or name.endswith(".h") ]

            cmakelist_txt_out = "set( " + directory + "_sources\n"
            for source in sorted(srcs):
                cmakelist_txt_out += " "*8 + "src/" + source + "\n"
            for source in sorted(model_srcs):
                cmakelist_txt_out += " "*8 + "src/" + source.split(directory + '/src/')[1] + "\n"
            cmakelist_txt_out += ")\n\n"

            cmakelist_txt_out += "set( " + directory + "_headers\n"
            for header in sorted(hdrs):
                cmakelist_txt_out += " "*8 + "include/" + header + "\n"
            for header in sorted(model_hdrs):
                cmakelist_txt_out += " "*8 + "include/" + header.split(directory + '/include/')[1] + "\n"
                
            cmakelist_txt_out += ")\n\n\
add_efit_library( " + directory + "_lib OPTION OBJECT SOURCES ${"+ directory +"_sources} HEADERS ${" + directory + "_headers} )\n\n"

            towrite = cmakelist_txt_out
            cmake = directory + "/CMakeLists.txt"
            candidate = directory + "/CMakeLists.txt.candidate"
            with open(candidate,"w") as f: f.write(towrite)
            update_only_if_different(cmake, candidate)
            
    ##################################################################
    #################### main CMakeList.txt ##########################
    ##################################################################
    
    main_hdrs = []
    main_srcs = []
    exe_txt_out = ""
    
    for f in listdir_nohidden("main"):
        if os.path.isfile("main/" + f):
            if f.endswith(".cpp") or f.endswith(".c") or f.endswith(".cc") or f.endswith(".cxx"):
                main_srcs.append(directory)
            if f.endswith(".hpp") or f.endswith(".h"):
                main_hdrs.append(directory)
    
    for directory in listdir_nohidden("main"):
        exe_srcs = []
        exe_hdrs = []
        if os.path.isdir("main/" + directory):
            exe_srcs = main_srcs
            exe_hdrs = main_hdrs
            exe_srcs = [ root + "/" + f for root,dirs,files in os.walk("main/" + directory) for f in files if f.endswith(".cpp") or f.endswith(".c") or f.endswith(".cc") or f.endswith(".cxx") ]
            exe_hdrs = [ root + "/" + f for root,dirs,files in os.walk("main/" + directory) for f in files if f.endswith(".hpp") or f.endswith(".h") ]
        
            exe_txt_out += "set( " + directory + "_hdrs\n"
            for header in exe_hdrs:
                exe_txt_out += " "*8 + directory + "/" + header.split("main/" + directory + "/")[1] + "\n"
            exe_txt_out += ")\n\n"
            exe_txt_out += "set( " + directory + "_srcs\n"
            for header in exe_srcs:
                exe_txt_out += " "*8 + directory + "/" + header.split("main/" + directory + "/")[1] + "\n"
            exe_txt_out += ")\n\n"
            
            exe_txt_out += "add_efit_executable( " + directory + " HEADERS ${" + directory +"_hdrs} SOURCES ${" + directory + "_srcs} )\n\n"
        
    towrite = exe_txt_out
    cmake = "main/CMakeLists.txt"
    candidate = "main/CMakeLists.txt.candidate"
    with open(candidate,"w") as f: f.write(towrite)
    update_only_if_different(cmake, candidate)
    
    #######################################################
    #################### main.cmake #######################
    #######################################################
    
    main_txt_output = "set (EFIT_INCLUDES\n" + " "*8 + "${EFIT_INCLUDES}\n"
    for d in directories:
        main_txt_output += " "*8 + "\"${PROJECT_SOURCE_DIR}/" + d + "/include\"\n"
        
    main_txt_output += ")\n\n"
    main_txt_output += "set (EFIT_OBJECTS\n" + " "*8 + "${EFIT_OBJECTS}\n"
    for d in directories:
        main_txt_output += " "*8 + "$<TARGET_OBJECTS:" + d + "_lib>\n"
        
    main_txt_output += ")\n\n"
    for d in directories:
        main_txt_output += "add_subdirectory(" + d + ")\n"
    main_txt_output += "add_subdirectory(main)\n"
    towrite = main_txt_output
    cmake = "cmake/main.cmake"
    candidate = "cmake/main.cmake.candidate"
    with open(candidate,"w") as f: f.write(towrite)
    update_only_if_different(cmake, candidate)
    
if __name__ == "__main__":
   main(sys.argv[1:])
