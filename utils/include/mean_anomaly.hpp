//  EFIT 6
//  *********************************************
///  \file
///
///  Mean Anomaly implemation
///
///  *********************************************
///
///  Authors (add name and date if you modify):
///
///  \author Gregory Martinez
///          (gregory.david.martinez@gmail.com)
///  \date 2016 May
///
///  *********************************************

#ifndef __MEAN_ANOMALY_HPP__
#define __MEAN_ANOMALY_HPP__

#include "constants.hpp"

namespace efit
{
    ///Solves 'E - e*sin(E) = dt' for E with e < 1.
    template <int N = 20>
    inline double BoundMeanAnomaly(double e, double dt)
    {
        if (e == 0)
            return dt;
        
        if (dt == 0.0)
            return 0.0;
        
        //int n = (dt > 0.0) ? int(dt/constants::_2pi + 0.5) : int(dt/constants::_2pi - 0.5);
        
        //double s = std::sqrt(8.0*(1.0-e)/e);
        //double E = s*std::sinh(std::asinh(3.0*(dt-n*constants::_2pi)/(1.0-e)/s)/3.0) + n*constants::_2pi;
        double E = dt + e * std::sin(dt) + e*e*std::sin(2.0*dt)/2.0;
        
        double dE, func, dfunc, ddfunc, dddfunc;
        
        for (int i = 0; i < N; i++)
        {
            dddfunc = e*std::cos(E);
            ddfunc = e*std::sin(E);
            dfunc = 1.0 - dddfunc;
            func = E - ddfunc - dt;
            
            dE = func*(6.0*dfunc*dfunc - 3.0*func*ddfunc)/(6.0*dfunc*(dfunc*dfunc - func*ddfunc) + func*func*dddfunc);
            //dE = 4.0*func*(6.0*dfunc*(dfunc*dfunc - func*ddfunc) + func*func*dddfunc)/(dfunc*dfunc*(24.0*dfunc*dfunc - 36.0*func*ddfunc) + func*func*(6.0*ddfunc*ddfunc + 8.0*dfunc*dddfunc + func*ddfunc));
            E -= dE;
            
            if (std::abs(dE/E) < 1.0e-9)
                return E;
        }
        
        //std::cout << "Max interations exceeded in BoundMeanAnomaly." << std::endl;
        
        return E;
    }
    
    ///Solves 'e*sinh(E) - E = dt' for E with e > 1.
    template<int N = 20>
    inline double UnBoundMeanAnomaly(double e, double dt)
    {
        if (dt == 0.0)
            return 0.0;
        
        double dE, func, dfunc, ddfunc, dddfunc;
        
        double s = std::sqrt(8.0*(e-1.0)/e);
        double E = s*std::sinh(std::asinh(3.0*dt/(e-1.0)/s)/3.0);
        
        for (int i = 0; i < N; i++)
        {
            dddfunc = e*std::cosh(E);
            ddfunc = e*std::sinh(E);
            dfunc = dddfunc - 1.0;
            func = ddfunc - E - dt;
            
            dE = func*(6.0*dfunc*dfunc - 3.0*func*ddfunc)/(6.0*dfunc*(dfunc*dfunc - func*ddfunc) + func*func*dddfunc);
            //dE = 4.0*func*(6.0*dfunc*(dfunc*dfunc - func*ddfunc) + func*func*dddfunc)/(dfunc*dfunc*(24.0*dfunc*dfunc - 36.0*func*ddfunc) + func*func*(6.0*ddfunc*ddfunc + 8.0*dfunc*dddfunc - func*ddfunc));
            E -= dE;
            
            if (std::abs(dE/E) < 1.0e-9)
                return E;
        }
        
        //std::cout << "Max interations exceeded in BoundMeanAnomaly." << std::endl;
        
        return E;
    }
    
    ///Solves (E^3)/6 + E/2 = dt for E with e = 1.
    inline double EqBoundMeanAnomaly(double dt)
    {
        return 2.0*std::sinh(std::asinh(3.0*dt)/3.0);
    }
}

#endif
