//  EFIT 6
//  *********************************************
///  \file
///
///  Base function or a efit model pointer
///
///  *********************************************
///
///  Authors (add name and date if you modify):
///
///  \author Gregory Martinez
///          (gregory.david.martinez@gmail.com)
///  \date 2016 May
///
///  *********************************************

#ifndef __EFIT_PTR_HPP__
#define __EFIT_PTR_HPP__

#include <memory>

namespace efit
{
    
    template <typename T>
    class efit_ptr : public std::shared_ptr< T >
    {
    private:
        typedef std::shared_ptr< T > s_ptr;
            
    public:
        efit_ptr(){}
        efit_ptr(const efit_ptr &in) : s_ptr (in){}
        efit_ptr(efit_ptr &&in) : s_ptr (std::move(in)) {}
        efit_ptr(T *in) : s_ptr(in) {}
        
        efit_ptr &operator=(T *in)
        {
                this->s_ptr::operator=(s_ptr(in));
        
                return *this;
        }
        
        efit_ptr &operator=(const efit_ptr &in)
        {
            this->s_ptr::operator=(in);
    
            return *this;
        }
        
        efit_ptr &operator=(s_ptr &&in)
        {
            this->s_ptr::operator=(std::move(in));
    
            return *this;
        }
        
        template<typename... U>
        void operator()(U&&... params)
        {
            return (*this)->operator()(std::forward<U>(params)...);
        }
        
    };
    
    template <typename T>
    inline efit_ptr<T> make_efit_ptr(T *ptr){return efit_ptr<T>(ptr);}
    
}

#endif
