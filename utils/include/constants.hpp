//  EFIT 6
//  *********************************************
///  \file constants.hpp
///
///  \brief Global constants used throughout the code
///
///   In this header are defined all the constants useful in the program. 
///
///  *********************************************
///
///  Authors (add name and date if you modify):
///
///  \author Gregory Martinez (gregory.david.martinez@gmail.com)
///          A. Hees (ahees@astro.ucla.edu)
///
///  \date 2016 May
///
///  *********************************************


#ifndef __CONSTANTS_HPP__
#define __CONSTANTS_HPP__
    
#include <boost/iterator/zip_iterator.hpp>
#include <boost/range.hpp>
#include <tuple>
#include <ctime>
    
namespace efit
{

		///  \struct constants
		///
		///  \brief contains all the constants usefull in the soft.
		///

    struct constants
    {
        //constexpr static long double pil = 3.141592653589793238L;
        constexpr static double pi = 3.141592653589793; ///< pi
        constexpr static double _2pi = 2.0*pi;          ///< 2pi */
        constexpr static double year = 31557600;        ///< "Julian year"=365.25 days in seconds. */
        constexpr static double au = 149597870700.0;    ///< AU in meters following IAU2012 conventions 
                                                        ///<   (see http://www.iau.org/static/resolutions/IAU2012_English.pdf) 
        constexpr static double GM_sun = 365.25*365.25*0.2959122082855911e-3; ///< Sun GM. Value taken from JPL DE430 ephemerides in AU^3/day^2 and transform into AU^3/yr^2 
                                                                                ///<     (see http://ipnpr.jpl.nasa.gov/progress_report/42-196/196C.pdf) */
        constexpr static double MGM_sun = 1.0e6*GM_sun;           ///< 10^6 GM_sun in AU^3/yr^2 
        constexpr static double MGM_sun_4pi2 = MGM_sun/pi/pi/4.0; ///< 10^6 GM_sun/4/pi^2 in AU^3/yr^2 
        constexpr static double c_mks = 299792458.0;              ///< speed of light in m/s 
        constexpr static double c = c_mks*year/au;                ///< speed of light in AU/yr 
        constexpr static double c_sq = c*c;                       ///< square of the speed of light in AU^2/yr^2
        constexpr static double auyr_to_kms = au/year/1000.0;     ///< transformation between AU/yr to km/s
        constexpr static double kms_to_auyr = 1000.0*year/au;     ///< transformation between km/s to AU/yr
        constexpr static double pc = 648000*au/pi;                ///< parsec defined as 648000 AU/pi such that as.pc=AU
        constexpr static double kpc = 1000.0*pc;                  ///< kiloparsec
        constexpr static double deg_to_rad = pi/180.0;            ///< tranformation from degree to radians
        
        constexpr static double tOrigin = 2000.0;                ///< Origin of time (in Julian year) for the black hole position and velocity
    };
    


    ///  \fn  inline double convertUTCToJYr(double time, int t0)
    ///  \brief transform UTC time (expressed in year) to Julian Year
 		///
 		///  \param[in] time input UTC time in year
 		///  \param[in] t0 input UTC reference time in year
    ///  \return the difference time - t0 in julian year
    ///
    ///  \warning NEED TO BE TESTED !
 		///

    inline double convertUTCToJYr(double time, int t0)
    {
        struct tm y1, y2, y3;
        double yr;
        double frac = std::modf(time, &yr);
        int n = int(yr) - t0;

        y1.tm_hour = y1.tm_min = y1.tm_mon = y1.tm_sec = y1.tm_yday = y1.tm_wday = 0;
        y1.tm_mday = 1;
        y1.tm_year = t0 - 1900;

        y2.tm_hour = y2.tm_min = y2.tm_mon = y2.tm_sec = y2.tm_yday = y2.tm_wday = 0;
        y2.tm_mday = 1;
        y2.tm_year = yr - 1900;
        
        y3.tm_hour = y3.tm_min = y3.tm_mon = y3.tm_sec = y3.tm_yday = y3.tm_wday = 0;
        y3.tm_mday = 1;
        y3.tm_year = yr - 1899;
        
        return (difftime(mktime(&y2), mktime(&y1)) + frac*difftime(mktime(&y2), mktime(&y3)))/3600.0/24.0/365.25;
    }
    
    ///  \fn  inline double convertUTCToMJD(double time, int t0)
    ///  \brief transform UTC time (expressed in year) to Modified Julian Day
 		///
 		///  \param[in] time input UTC time in year
    ///  \return the time converted into Modified Julian Days
    ///
    ///  \warning NEED TO BE TESTED !
    ///  \warning leapseconds neglected.
 		///
    
    inline double convertUTCToMJD(double time)
    {
        struct tm y1, y2, y3;
        double yr;
        double frac = std::modf(time, &yr);

        y1.tm_hour = y1.tm_min = y1.tm_sec = y1.tm_yday = y1.tm_wday = 0;
        y1.tm_mday = 17;
        y1.tm_mon = 10;
        y1.tm_year = -42;

        y2.tm_hour = y2.tm_min = y2.tm_mon = y2.tm_sec = y2.tm_yday = y2.tm_wday = 0;
        y2.tm_mday = 1;
        y2.tm_year = yr - 1900;
        
        y3.tm_hour = y2.tm_min = y2.tm_mon = y2.tm_sec = y2.tm_yday = y2.tm_wday = 0;
        y3.tm_mday = 1;
        y3.tm_year = yr - 1899;
        
        return (difftime(mktime(&y2), mktime(&y1)) + frac*difftime(mktime(&y2), mktime(&y3)))/3600.0/24.0;
    }
    
    ///  \fn  constexpr double SQ(double a)
    ///  \brief return the square of a double
 		///
 		///  \param[in] a double
    ///  \return the square of a
    ///

    constexpr double SQ(double a){return a*a;}

    ///  \fn  template <typename... T> inline auto zip(const T&... containers)
    ///  \brief to be added
 		///
 		///   \return to be added!
 		///
    template <typename... T>
    inline auto zip(const T&... containers) -> boost::iterator_range<boost::zip_iterator<decltype(boost::make_tuple(std::begin(containers)...))>>
    {
        return boost::make_iterator_range(
            boost::make_zip_iterator(boost::make_tuple(std::begin(containers)...)),
            boost::make_zip_iterator(boost::make_tuple(std::end(containers)...)));
    }
}

#endif
