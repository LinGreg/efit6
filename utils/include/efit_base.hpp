//  EFIT 6
//  *********************************************
///  \file
///
///  Base Function to a efit model
///
///  *********************************************
///
///  Authors (add name and date if you modify):
///
///  \author Gregory Martinez
///          (gregory.david.martinez@gmail.com)
///  \date 2016 May
///
///  *********************************************

#ifndef __EFIT_BASE_HPP__
#define __EFIT_BASE_HPP__

#include <memory>

namespace efit
{
    
    class efit_base : public std::enable_shared_from_this<efit_base>
    {
    private:
        std::vector<std::string> global_tags;
        std::vector<std::string> local_tags;
        
    public:
        template <typename T1, typename T2> 
        using map_type = std::unordered_map<T1, T2>;
        template <typename T> 
        using vec_type  = std::vector<T>;
        
        std::vector<std::string> GlobalTags() const {return global_tags;}
        std::vector<std::string> LocalTags() const {return local_tags;}
        void AddGlobalTags(const std::vector<std::string> &input) 
        {
            global_tags.insert(global_tags.end(), input.begin(), input.end());
        }
        void AddLocalTags(const std::vector<std::string> &input) 
        {
            local_tags.insert(local_tags.end(), input.begin(), input.end());
        }
        
        std::map<std::string, double> initDer(double value = 0.0)
        {
          std::map<std::string, double> map;
          for (auto &&elem : global_tags) 
          {
            map[elem]=value;
          }
          for (auto &&elem : local_tags) 
          {
            map[elem]=value;
          }
          
          return map;
        }
        
        virtual ~efit_base() {}
    };
    
}

#endif
