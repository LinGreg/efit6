//  EFIT 6
//  *********************************************
///  \file
///
///  efit utilities
///
///  *********************************************
///
///  Authors (add name and date if you modify):
///
///  \author Gregory Martinez
///          (gregory.david.martinez@gmail.com)
///  \date 2016 May
///
///  *********************************************

#ifndef __FILE_UTILS_HPP__
#define __FILE_UTILS_HPP__

#include <sstream>
#include <iostream>
#include <fstream>
#include <string>
#include <type_traits>
#include <map>
#include <unordered_map>
#include <set>
#include <unordered_set>
#include <vector>
#include <list>
#include <forward_list>
#include <deque>
#include <array>

#include "yaml-cpp/yaml.h"

namespace efit
{

    template<typename T, typename U>
    inline void EnterGlobalMap(const T &input, std::unordered_map<std::string, U> &map, const YAML::Node &node)
    {
        for (auto &&elem: input)
        {
            for (const std::string &elm : elem.model_ptr->GlobalTags())
            {
                if (node["parameters"] && node["parameters"].IsMap())
                {
                    if (node["parameters"][elm])
                    {
                        map[elm] = node["parameters"][elm].as<U>();
                    }
                    else
                    {
                        std::cout << elem.model_name << " needs global param:  " << elm << std::endl;
                        exit(-1);
                    }
                }
                else
                {
                    std::cout << "Did not specify global 'parameters'" << std::endl;
                }
            }
        }
    }

    inline void EnterEFitData(const std::string &file, const std::function<void (const std::vector<double> &)> &func)
    {
        std::ifstream in(file.c_str());
        if (!in.is_open())
        {
            std::cout << "Cannot open " << file << std::endl;
            exit(-1);
        }
        
        std::string line;
        while (std::getline(in, line))
        {
            line = line.substr(0, line.find_first_of("#"));
            std::stringstream ss(line);
            std::vector<double> values;
            double val;
            
            while(ss >> val)
            {
                values.push_back(val);
            }
            
            func(values);
        }
    }
    
    /////////////////////
    //remove_all
    /////////////////////
    
    template <typename T>
    struct remove_all
    {
        typedef typename std::remove_cv
        <
            typename std::remove_volatile
            <
                typename std::remove_const
                <
                    typename std::remove_reference
                    <
                        T
                    >::type
                >::type
            >::type
        >::type type;
    };
    
    /////////////////////
    //is_container
    /////////////////////

    template <typename T>
    struct __is_container__
    {
        static const bool value = false;
        typedef T type;
    };

    template <typename T>
    struct __is_container__<std::vector<T>>
    {
        static const bool value = true;
        typedef T type;
    };

    template <typename T>
    struct __is_container__<std::set<T>>
    {
        static const bool value = true;
        typedef T type;
    };
    
    template <typename T>
    struct __is_container__<std::multiset<T>>
    {
        static const bool value = true;
        typedef T type;
    };

    template <typename T1, typename T2>
    struct __is_container__<std::map<T1, T2>>
    {
        static const bool value = true;
        typedef std::pair<T1, T2> type;
    };

    template <typename T1, typename T2>
    struct __is_container__<std::multimap<T1, T2>>
    {
        static const bool value = true;
        typedef std::pair<T1, T2> type;
    };
    
    template <typename T1, typename T2>
    struct __is_container__<std::unordered_map<T1, T2>>
    {
        static const bool value = true;
        typedef std::pair<T1, T2> type;
    };
    
    template <typename T1, typename T2>
    struct __is_container__<std::unordered_multimap<T1, T2>>
    {
        static const bool value = true;
        typedef std::pair<T1, T2> type;
    };

    template <typename T>
    struct __is_container__<std::unordered_set<T>>
    {
        static const bool value = true;
        typedef T type;
    };
    
    template <typename T>
    struct __is_container__<std::unordered_multiset<T>>
    {
        static const bool value = true;
        typedef T type;
    };

    template <typename T>
    struct __is_container__<std::deque<T>>
    {
        static const bool value = true;
        typedef T type;
    };
    
    template <typename T, size_t N>
    struct __is_container__<std::array<T, N>>
    {
        static const bool value = true;
        typedef T type;
    };
    
    template <typename T>
    struct __is_container__<std::list<T>>
    {
        static const bool value = true;
        typedef T type;
    };

    template <typename T>
    struct __is_container__<std::forward_list<T>>
    {
        static const bool value = true;
        typedef T type;
    };

    template <typename T>
    struct is_container
    {
        const static bool value = __is_container__<typename remove_all<T>::type>::value;
        typedef typename __is_container__<typename remove_all<T>::type>::type type;
    };
    
    /////////////////////
    //is_vector
    /////////////////////
    
    template <typename T>
    struct __is_vector__
    {
        static const bool value = false;
        typedef T type;
    };
    
    template <typename T>
    struct __is_vector__<std::vector<T>>
    {
        static const bool value = true;
        typedef T type;
    };
    
    template <typename T>
    struct is_vector
    {
        const static bool value = __is_vector__<typename remove_all<T>::type>::value;
        typedef typename __is_vector__<typename remove_all<T>::type>::type type;
    };
    
    /////////////////////
    //is_pair
    /////////////////////
    
    template <typename T>
    struct __is_pair__
    {
            const static bool value = false;
            typedef T first_type;
            typedef T second_type;
    };

    template <typename T1, typename T2>
    struct __is_pair__ <std::pair<T1, T2>>
    {
            const static bool value = true;
            typedef T1 first_type;
            typedef T2 second_type;
    };
                    
    template <typename T>
    struct is_pair
    {
            const static bool value = __is_pair__<typename remove_all<T>::type>::value;
            typedef typename __is_pair__<typename remove_all<T>::type>::first_type first_type;
            typedef typename __is_pair__<typename remove_all<T>::type>::second_type second_type;
    };
    
    /********************************/
    /****** Stream Operators ********/
    /********************************/
    
    template <typename T>
    inline typename std::enable_if <is_container<T>::value, std::ostream &>::type
    operator << (std::ostream &out, const T &in)
    {
        if (in.size() == 0)
            return out << "[]";
        
        auto it = in.begin();
        auto end = in.end();
        out << "[" << *it;
        for (++it; it != end; ++it)
        {
            out << ", " << *it;
        }
        
        return out << "]";
    }
    
    template <typename T>
    inline typename std::enable_if <is_pair<T>::value, std::ostream &>::type
    operator << (std::ostream &out, const T &in)
    {
        return out << "{" << in.first << " : " << in.second << "}";
    }
}

#endif
