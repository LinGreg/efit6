//  EFIT 6
//  *********************************************
///  \file
///
///  Levenberg implemation
///
///  *********************************************
///
///  Authors (add name and date if you modify):
///
///  \author Gregory Martinez
///          (gregory.david.martinez@gmail.com)
///  \date 2016 May
///
///  *********************************************

#ifndef __levenberg__
#define __levenberg__

#include <functional>
#include <vector>
#include <cmath>

namespace efit
{
    
    void gaussj(std::vector<std::vector<double>> &, int, std::vector<std::vector<double>> &, int m);

    class LevenMarq
    {
        private:
            double chisq;
            double ochisq;
            double alamda;
            /*double **alpha;
            double **covar;
            double *beta;
            double *a;
            double *atry;
            double *da;
            double **oneda;
            double *eigVal;
            double **eigVec;*/
            std::vector<std::vector<double>> alpha, covar, oneda, eigVec;
            std::vector<double> beta, a, atry, da, eigVal;
            int ma;
            int Ntot;
            double tol;
            bool accepted;
            //double (LevenMarq::*findCof)(double *, double *, double **);
            std::function <double (std::vector<double> &, std::vector<double> &, std::vector<std::vector<double>> &)> findCof;
            
        public:
            LevenMarq(const double, const int);
            //void LMFindMin(double *, const int, double (LevenMarq::*)(double *, double *, double **));
            void LMFindMin(std::vector<double> &, const std::function <double (std::vector<double> &, std::vector<double> &, std::vector<std::vector<double>> &)> &);
            void Calc();
            void FindCof(std::vector<double> &ain);
    };

}

#endif
