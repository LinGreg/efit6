#include "levenberg.hpp"

namespace efit
{
    template<typename T>
    inline void SWAP(T &a, T &b) {double dum=a; a=b; b=dum;}
    
    LevenMarq::LevenMarq(const double tolin, const int Ntotin) : Ntot(Ntotin), tol(tolin)
    {
    }

    void LevenMarq::FindCof(std::vector<double> &ain)
    {
        int i, j;
        for (i = 0; i < ma; i++)
        {
            for (j = 0; j < ma; j++)
                alpha[i][j] = 0.0;
            beta[i] = 0.0;
        }
        chisq=findCof(ain, beta, alpha);
        return;
    }

    void LevenMarq::Calc()
    {
        int j,k,l;

        if (alamda < 0.0) {
            alamda=0.001;
            FindCof(a);
            ochisq=chisq;
            for (j=0;j<ma;j++) atry[j]=a[j];
        }
        for (j=0;j<ma;j++) {
            for (k=0;k<ma;k++) covar[j][k]=alpha[j][k];
            covar[j][j]=alpha[j][j]*(1.0+alamda);
            oneda[j][0]=beta[j];    
        }
        gaussj(covar,ma,oneda,1);
        for (j=0;j<ma;j++) da[j]=oneda[j][0];
        if (alamda == 0.0) {
            return;
        }
        for (j=0,l=0;l<ma;l++)
        {
            atry[l]=a[l]+da[j++];
        }
        FindCof(atry);
        if (chisq < ochisq) {
            alamda *= 0.1;
            ochisq=chisq;
            for (j=0;j<ma;j++) {
                for (k=0;k<ma;k++) alpha[j][k]=covar[j][k];
                beta[j]=da[j];
            }
            for (l=0;l<ma;l++) a[l]=atry[l];
            accepted = true;
        } else {
            alamda *= 10.0;
            chisq=ochisq;
            accepted = false;
        }
        
        return;
    }

    void LevenMarq::LMFindMin(std::vector<double> &a0, const std::function <double (std::vector<double> &, std::vector<double> &, std::vector<std::vector<double>> &)> &fin)
    {
        a = a0;
        ma = a0.size();
        alpha = std::vector<std::vector<double>>(ma, std::vector<double>(ma));
        covar = std::vector<std::vector<double>>(ma, std::vector<double>(ma));
        beta = std::vector <double> (ma);
        atry = std::vector <double> (ma);
        da = std::vector <double> (ma);
        oneda = std::vector<std::vector<double>>(ma, std::vector<double>(1));
        findCof = fin;
        double chi2old;
        alamda = -1.0;
        Calc();
        int count=0;

        do
        {
            chi2old = chisq;
            Calc();
            if (alamda > 1e100)
                break;
            if (accepted)
            {
                if ((chi2old - chisq)/chisq < tol)
                    count++;
            }
            else
            {
                if (count != 0)
                    count = 0;
            }
        }
        while(count != Ntot);
    }

    void gaussj(std::vector<std::vector<double>> &a, int n, std::vector<std::vector<double>> &b, int m)
    {
        int i,icol=0,irow=0,j,k,l,ll;
        double big,dum,pivinv;

        std::vector<int> indxc(n);
        std::vector<int> indxr(n);
        std::vector<int> ipiv(n);
        for (j=0;j<n;j++) ipiv[j]=0;
        for (i=0;i<n;i++) {
            big=0.0;
            for (j=0;j<n;j++)
                if (ipiv[j] != 1)
                    for (k=0;k<n;k++) {
                        if (ipiv[k] == 0) {
                            if (std::abs(a[j][k]) >= big) {
                                big=fabs(a[j][k]);
                                irow=j;
                                icol=k;
                            }
                        }
                    }
                    ++(ipiv[icol]);
                    if (irow != icol) {
                        for (l=0;l<n;l++) SWAP(a[irow][l],a[icol][l]);
                        for (l=0;l<m;l++) SWAP(b[irow][l],b[icol][l]);
                    }
                    indxr[i]=irow;
                    indxc[i]=icol;
                    //if (a[icol][icol] == 0.0) cout << "gaussj: Singular Matrix-2" << endl;
                    pivinv=1.0/a[icol][icol];
                    a[icol][icol]=1.0;
                    for (l=0;l<n;l++) a[icol][l] *= pivinv;
                    for (l=0;l<m;l++) b[icol][l] *= pivinv;
                    for (ll=0;ll<n;ll++)
                        if (ll != icol) {
                        dum=a[ll][icol];
                        a[ll][icol]=0.0;
                        for (l=0;l<n;l++) a[ll][l] -= a[icol][l]*dum;
                        for (l=0;l<m;l++) b[ll][l] -= b[icol][l]*dum;
                        }
        }
        for (l=n-1;l>=0;l--) {
            if (indxr[l] != indxc[l])
                for (k=0;k<n;k++)
                    SWAP(a[k][indxr[l]],a[k][indxc[l]]);
        }
    }

}
