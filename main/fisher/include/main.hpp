//  EFIT 6
//  *********************************************
///  \file
///
///  declaration for scanner module
///
///  *********************************************
///
///  Authors (add name and date if you modify):
///
///  \author Gregory Martinez
///          (gregory.david.martinez@gmail.com)
///  \date 2016 May
///
///  \author Arilien Hees
///          (p.scott@imperial.ac.uk)   
///  \date 2016 May
///
///  *********************************************

#ifndef __MAIN_CPP__
#define __MAIN_CPP__

#include "file_utils.hpp"
#include "cholesky.hpp"

using namespace efit;

struct efit_struct
{
    efit::orbits::orbits_ptr model_ptr;
    std::string model_name;
    std::unordered_map<std::string, YAML::Node> local_nodes;
    std::unordered_map<std::string, double> local_map;
    std::vector<std::reference_wrapper<double>> local_vec;
    
    efit_struct(const std::string &name, const YAML::Node &node) : model_name(name)
    {
        if (node && node.IsMap())
        {
            if (node["type"] && node["type"].IsScalar())
            {
                std::string type = node["type"].as<std::string>();
                model_ptr = efit::orbits::orbits_factory.at(type)(node);
                
                if (node["parameters"] && node["parameters"].IsMap())
                {
                    for (auto &&elm : model_ptr->LocalTags())
                    {
                        if (node["parameters"][elm])
                        {
                            if (node["parameters"][elm].IsScalar())
                                local_map[elm] = node["parameters"][elm].as<double>();
                            
                            local_nodes[elm] = node["parameters"][elm];
                        }
                        else
                        {
                            std::cout << "did not enter " << name << " param:  " << elm << std::endl;
                            exit(-1);
                        }
                    }
                    
                    for (auto &&elm : model_ptr->LocalTags())
                    {
                        local_vec.push_back(std::ref(local_map[elm]));
                    }
                }
                else
                {
                    std::cout << "Did not specify 'parameters' for " << name << std::endl;
                    exit(-1);
                }
            }
            else
            {
                std::cout << "'type' not specified for " << name << std::endl;
                exit(-1);
            }
        }
        else
        {
            std::cout << "yaml file is not in the correct format" << std::endl;
            exit(-1);
        }
        
    }

};

struct fisher_struct : public efit_struct
{
    typedef struct{double time, x, y, xerr, yerr;} pos_type;
    typedef struct{double time, velocity, verr;} vel_type;
    std::vector<pos_type> pos_data;
    std::vector<vel_type> vel_data;
    std::vector<efit::orbits::orbits_base::dposition_type> pos_obs;
    std::vector<efit::orbits::orbits_base::dvelocity_type> vel_obs;
    std::vector<std::vector<std::reference_wrapper<double>>> dx_local, dy_local, dvel_local, dx_global, dy_global, dvel_global;
    std::vector<std::reference_wrapper<double>> x_vec, y_vec, vel_vec;
    std::string file;
    int offset;
    
    fisher_struct(const std::string &name, const YAML::Node &node) : efit_struct(name, node)
    {
        file = (node["file"] && node["file"].IsScalar()) ? node["file"].as<std::string>() : name;
            
        EnterEFitData(file + ".points", [&](const std::vector<double> &values)
        {
            pos_type dat;
            if (values.size() == 5)
            {
                dat.time = values[0];
                dat.x = values[1]*1000.0;
                dat.y = values[2]*1000.0;
                dat.xerr = values[3]*1000.0;
                dat.yerr = values[4]*1000.0;
            }
            else
            {
                std::cout << (file + ".points") << " file in unrecognized format" << std::endl;
                exit(-1);
            }
            
            pos_data.push_back(dat);
        });
        
        EnterEFitData(file + ".rv", [&](const std::vector<double> &values)
        {
            vel_type dat;
            if (values.size() == 3)
            {
                dat.time = values[0];
                dat.velocity = values[1];
                dat.verr = values[2];
            }
            else
            {
                std::cout << (file + ".rv") << " file in unrecognized format" << std::endl;
                exit(-1);
            }
            
            vel_data.push_back(dat);
        });
        
        for (auto &&pos : pos_data)
        {
            pos_obs.push_back({pos.time, pos.x, pos.y});
        }
        
        for (auto &&vel : vel_data)
        {
            vel_obs.push_back({vel.time, vel.velocity});
        }
        
    }
    
    size_t size() const {return model_ptr->LocalTags().size();}
    
    void InputMap(const std::vector<std::string> &globals, int offset_in)
    {
        auto &&locals = model_ptr->LocalTags();
        offset = offset_in;
        
        for (auto &&pos : pos_obs)
        {
            x_vec.push_back(pos.x);
            y_vec.push_back(pos.y);
            
            for (auto &&tag : globals)
            {
                pos.dx[tag] = 0.0;
                pos.dy[tag] = 0.0;
            }
            
            for (auto &&tag : locals)
            {
                pos.dx[tag] = 0.0;
                pos.dy[tag] = 0.0;
            }
            
            std::vector<std::reference_wrapper<double>> tempxl, tempyl, tempxg, tempyg;
            
            for (auto &&tag : globals)
            {
                tempxg.push_back(pos.dx[tag]);
                tempyg.push_back(pos.dy[tag]);
            }
            
            for (auto &&tag : locals)
            {
                tempxl.push_back(pos.dx[tag]);
                tempyl.push_back(pos.dy[tag]);
            }
            
            dx_local.push_back(tempxl);
            dy_local.push_back(tempyl);
            dx_global.push_back(tempxg);
            dy_global.push_back(tempyg);
        }
        
        for (auto &&vel : vel_obs)
        {
            vel_vec.push_back(vel.velocity);
            
            for (auto &&tag : globals)
            {
                vel.dvelocity[tag] = 0.0;
            }
            
            for (auto &&tag : locals)
            {
                vel.dvelocity[tag] = 0.0;
            }
            
            std::vector<std::reference_wrapper<double>> tempvl, tempvg;
            
            for (auto &&tag : globals)
            {
                tempvg.push_back(std::ref(vel.dvelocity[tag]));
            }
            
            for (auto &&tag : locals)
            {
                tempvl.push_back(std::ref(vel.dvelocity[tag]));
            }
            
            dvel_local.push_back(tempvl);
            dvel_global.push_back(tempvg);
        }
    }
    
    void Calc(std::unordered_map<std::string, double> &global_map)
    {
        model_ptr(global_map, local_map, pos_obs, vel_obs);
    }
    
    double operator()(std::vector<double> &beta, std::vector<std::vector<double>> &alpha)
    {
        double chi2 = 0.0;
        for (int i = 0, end = pos_data.size(); i < end; i++)
        {
            double dx = (pos_obs[i].x - pos_data[i].x)/pos_data[i].xerr;
            double dy = (pos_obs[i].y - pos_data[i].y)/pos_data[i].yerr;
            chi2 += dx*dx + dy*dy;
            
            for (int j = 0, endj = dx_global[i].size(); j < endj; j++)
            {
                beta[j] += dx*dx_global[i][j]/pos_data[i].xerr + dy*dy_global[i][j]/pos_data[i].yerr;
                for (int k = 0; k < endj; k++)
                {
                    alpha[j][k] += dx_global[i][j]*dx_global[i][j]/pos_data[i].xerr/pos_data[i].xerr + dy_global[i][j]*dy_global[i][k]/pos_data[i].yerr/pos_data[i].yerr;
                }
            }
            
            for (int j = 0, endj = dx_local[i].size(); j < endj; j++)
            {
                beta[j+offset] += dx*dx_local[i][j]/pos_data[i].xerr + dy*dy_local[i][j]/pos_data[i].yerr;
                for (int k = 0; k < endj; k++)
                {
                    alpha[j+offset][k+offset] += dx_local[i][j]*dx_local[i][k]/pos_data[i].xerr/pos_data[i].xerr + dy_local[i][j]*dy_local[i][k]/pos_data[i].yerr/pos_data[i].yerr;
                }
            }
        }
        
        for (int i = 0, end = vel_data.size(); i < end; i++)
        {
            double dvel = (vel_obs[i].velocity - vel_data[i].velocity)/vel_data[i].verr;
            chi2 += dvel*dvel;
            
            for (int j = 0, endj = dvel_global[i].size(); j < endj; j++)
            {
                beta[j] += dvel*dvel_global[i][j]/vel_data[i].verr;
                for (int k = 0; k < endj; k++)
                {
                    alpha[j][k] += dvel_global[i][j]*dvel_global[i][k]/vel_data[i].verr/vel_data[i].verr;
                }
            }
            
            for (int j = 0, endj = dvel_local[i].size(); j < endj; j++)
            {
                beta[j+offset] += dvel*dvel_local[i][j]/vel_data[i].verr;
                for (int k = 0; k < endj; k++)
                {
                    alpha[j+offset][k+offset] += dvel_local[i][j]*dvel_local[i][k]/vel_data[i].verr/vel_data[i].verr;
                }
            }
        }
        
        return chi2;
    }
};

inline std::vector<std::vector<double>> CalcCov(std::vector<fisher_struct> &vec, double &chi2, int size)
{
    std::vector<std::vector<double>> alpha(size, std::vector<double>(size, 0.0));
    std::vector<double> beta(size, 0.0);
    
    chi2 = 0.0;
    for (auto &&elem : vec)
    {
        chi2 += elem(beta, alpha);
    }
    
    Cholesky chol(alpha);
    
    return chol.Inverse();
}

#endif
