
#include "orbits.hpp"
#include "main.hpp"
#include "levenberg.hpp"

int main(int argc, char **argv)
{
    if (argc != 2)
    {
        std::cout << "not input file specified." << std::endl;
        exit(-1);
    }
    
    YAML::Node node = YAML::LoadFile(argv[1]);
    
    if(!node.IsMap())
    {
        std::cout << "ini file not in correct format" << std::endl;
        exit(-1);
    }
    
    std::vector<fisher_struct> models;
    std::unordered_map<std::string, double> globals;
    std::vector<std::string> global_tags, total_tags;
    std::vector<std::reference_wrapper<double>> point;
    
    for (auto &&mod : node)
    {
        std::string name = mod.first.as<std::string>();
        if (name != "global")
        {
            models.emplace_back(name, mod.second);
        }
    }
    
    EnterGlobalMap(models, globals, node["global"]);
    
    for (auto &&vec : globals)
    {
        point.push_back(std::ref(vec.second));
        global_tags.push_back(vec.first);
    }
    total_tags = global_tags;
    int offset = global_tags.size();
    for (auto &&mod : models)
    {
        mod.InputMap(global_tags, offset);
        offset += mod.size();
        
        for (auto &&vec : mod.local_vec)
        {
            point.push_back(std::ref(vec));
        }
        auto &&vec = mod.model_ptr->LocalTags();
        total_tags.insert(total_tags.end(), vec.begin(), vec.end());
    }
    
    std::vector<double> pt(point.begin(), point.end());
    LevenMarq levenberg(1.0e-4, 1000);
    levenberg.LMFindMin(pt, 
    [&](std::vector<double> &a, std::vector<double> &beta, std::vector<std::vector<double>> &alpha)
    {
        double chi2 = 0.0;
        for (auto &&tup : zip(point, a))
        {
            tup.get<0>().get() = tup.get<1>();
        }
        
        for (auto &&elem : models)
        {
            elem.Calc(globals);
            chi2 += elem(beta, alpha);
        }
        
        return chi2;
    });
    
    std::ofstream out("cov");
    out << "point = " << point << std::endl;
    double chi2;
    auto cov = CalcCov(models, chi2, point.size());
    
    
    out << "parameters = " << total_tags << std::endl;
    out << "chi2 = " << chi2 << std::endl;
    out << "cov = " << cov << std::endl;
}
