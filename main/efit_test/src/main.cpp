#include <iostream>
#include "orbits.hpp"
#include "main.hpp"


using namespace efit::orbits;

int main(int argc, char **args)
{
    if (argc != 2)
    {
        std::cout << "you need to provide a file." << std::endl;
        return -1;
    }
    std::cout.precision(17);
    std::cout << efit::constants::GM_sun/efit::constants::year/efit::constants::year*efit::constants::au*efit::constants::au*efit::constants::au << "\n";
    //Entering orbit_type, global and local maps from yaml file.
    YAML::Node node = YAML::LoadFile(args[1]);
    std::unordered_map<std::string, double> global_map, local_map;
    std::string orbit_type;
    
    if (node["orbit_type"] && node["orbit_type"].IsScalar())
    {
        orbit_type = node["orbit_type"].as<std::string>();
    }
    else
    {
        std::cout << "did not specify orbit type" << std::endl;
        return -1;
    }
    
    //Actual orbit stuff
    orbits_ptr orbit = orbits_factory.at(orbit_type)(YAML::Node());
    // or orbit_base * orbits = orbits_factory[orbit_type](YAML::Node());
        
    global_map = EnterMap("global", orbit->GlobalTags(), node);
    local_map = EnterMap("local", orbit->LocalTags(), node);
    
    if (global_map.size() == 0 or local_map.size() == 0)
    {
        std::cout << "Input yaml file is not configured properly" << std::endl;
        return -1;
    }
    
    std::vector<orbits_base::position_type> pos(1);
    std::vector<orbits_base::velocity_type> vel(1);
    pos[0].time = 2012.0;
    vel[0].time = 2014.0;
    
    orbit(global_map, local_map, pos, vel);
    
    std::cout << "x = " << pos[0].x << "\n";
    std::cout << "y = " << pos[0].y << "\n";
    std::cout << "v = " << vel[0].velocity << "\n";
}
