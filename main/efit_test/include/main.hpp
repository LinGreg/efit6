#ifndef __MAIN_HPP__
#define __MAIN_HPP__

inline std::unordered_map<std::string, double> EnterMap(std::string name, const std::vector<std::string> vec_in, const YAML::Node &node)
{
    std::unordered_map<std::string, double> temp;
    
    if (node["global"] && node["global"].IsMap())
    {
        for (auto &&vec : vec_in)
        {
            if (node[name][vec])
                temp[vec] = node[name][vec].as<double>();
            else
            {
                std::cout << "did not enter " << name << " param:  " << vec << std::endl;
            }
        }
    }

    return temp;
}

#endif
