#include <iostream>
#include <iomanip>
#include <chrono>
#include <random>

#include "orbits.hpp"
#include "main.hpp"

using namespace efit::orbits;

int main(int argc, char **argv)
{
    if (argc != 2)
    {
        std::cout << "need input file." << std::endl;
        exit(-1);
    }
    
    unsigned long long seed = std::chrono::system_clock::now().time_since_epoch().count();
    std::mt19937_64 random(seed);
    std::normal_distribution<double> dev (0.0,1.0);
    std::vector<input_type> local_stuff;
    std::unordered_map<std::string, double> global_map;
    std::string global_name;
    bool simulate = true;
    YAML::Node node = YAML::LoadFile(argv[1]);
    
    if (!node.IsMap())
    {
        std::cout << "input file not the correct format" << std::endl;
        exit(-1);
    }
    
    for (auto &&nod : node)
    {
        if(nod.second.IsScalar() && nod.first.as<std::string>() == "simulate_errors")
        {
            simulate = nod.second.as<bool>();
        }
        else
        {
            input_type input;
            InputMap(nod.first.as<std::string>(), input, node);
            if (std::get<1>(input) == "global")
            {
                global_name = nod.first.as<std::string>();
            }
            else
            {
                local_stuff.push_back(input);
            }
        }
    }
    
    EnterGlobalMap(global_name, local_stuff, global_map, node);
    
    for (auto &&elem : local_stuff)
    {
        std::vector<orbits_base::dposition_type> pos_obs;
        std::vector<orbits_base::dvelocity_type> vel_obs;
        std::vector<pos_type> pos;
        std::vector<vel_type> vel;
        EnterVecs(std::get<1>(elem), pos, vel);
        
        for (auto &&p : pos)
        {
            pos_obs.push_back({p.time, 0, 0});
        }
        
        for (auto &&v : vel)
        {
            vel_obs.push_back({v.time, 0});
        }
        
        std::get<0>(elem)(global_map, std::get<2>(elem), pos_obs, vel_obs);
        
        std::cout << "simulating " << (simulate ? "with" : "without") << " errors..." << std::flush;
        
        std::ofstream p_out((std::get<1>(elem) + ".mock.points").c_str());
        for (int j = 0, end = pos.size(); j < end; j++)
        {
            auto &&d = pos[j];
            d.x = pos_obs[j].x + (simulate ? d.xerr*dev(random) : 0.0);
            d.y = pos_obs[j].y + (simulate ? d.yerr*dev(random) : 0.0);
            p_out << std::setprecision(12) << std::setiosflags(std::ios::left) << std::setw(18) << d.time << std::setw(18) << d.x/1000.0 << std::setw(18) << d.y/1000.0 << std::setw(18) << d.xerr/1000.0 << d.yerr/1000.0 << std::endl;
        }
        
        std::ofstream v_out((std::get<1>(elem) + ".mock.rv").c_str());
        for (int j = 0, end = vel.size(); j < end; j++)
        {
            auto &&d = vel[j];
            d.velocity = vel_obs[j].velocity + (simulate ? d.verr*dev(random) : 0.0);
            v_out << std::setprecision(12) << std::setiosflags(std::ios::left) << std::setw(18) << d.time << std::setw(18) << d.velocity << d.verr << std::endl;
        }
        
        std::cout << "done." << std::endl;
    }
}
