#ifndef __MAIN_CPP__
#define __MAIN_CPP__

#include <tuple>
#include <sstream>
#include <fstream>

typedef std::tuple<efit::orbits::orbits_ptr, std::string, std::unordered_map<std::string, double>> input_type;

inline void InputMap(const std::string &name, input_type &output, const YAML::Node &node)
{
    if (node[name] && node[name].IsMap())
    {
        if (node[name]["type"] && node[name]["type"].IsScalar())
        {
            std::string type = node[name]["type"].as<std::string>();
            if (type == "global")
            {
                std::get<0>(output) = 0;
                std::get<1>(output) = "global";
            }
            else
            {
                std::get<0>(output) = efit::orbits::orbits_factory.at(type)(node[name]);
                if (node[name]["file"] && node[name]["file"].IsScalar())
                    std::get<1>(output) = node[name]["file"].as<std::string>();
                else
                    std::get<1>(output) = name;
                
                if (node[name]["parameters"] && node[name]["parameters"].IsMap())
                {
                    for (auto &&elm : std::get<0>(output)->LocalTags())
                    {
                        if (node[name]["parameters"][elm])
                            std::get<2>(output)[elm] = node[name]["parameters"][elm].as<double>();
                        else
                        {
                            std::cout << "did not enter " << name << " param:  " << elm << std::endl;
                            exit(-1);
                        }
                    }
                }
                else
                {
                    std::cout << "Did not specify 'parameters' for " << name << std::endl;
                }
            }
        }
        else
        {
            std::cout << "'type' not specified for " << name << std::endl;
            exit(-1);
        }
    }
    else
    {
        std::cout << "yaml file is not in the correct format" << std::endl;
        exit(-1);
    }
}

inline void EnterGlobalMap(const std::string &name, const std::vector<input_type> input, std::unordered_map<std::string, double> &map, const YAML::Node &node)
{
    for (auto &&elem: input)
    {
        for (auto &&elm : std::get<0>(elem)->GlobalTags())
        {
            if (node[name]["parameters"] && node[name]["parameters"].IsMap())
            {
                if (node[name]["parameters"][elm] && node[name]["parameters"][elm].IsScalar())
                {
                    map[elm] = node[name]["parameters"][elm].as<double>();
                }
                else
                {
                    std::cout << std::get<1>(elem) << " needs global param:  " << elm << std::endl;
                    exit(-1);
                }
            }
            else
            {
                std::cout << "Did not specify global 'parameters'" << std::endl;
            }
        }
    }
}

typedef struct{double time, x, y, xerr, yerr;} pos_type;
typedef struct{double time, velocity, verr;} vel_type;

inline void EnterVecs(const std::string &file, std::vector<pos_type> &pos_in, std::vector<vel_type> &vel_in)
{
    std::ifstream in((file + ".points").c_str());
    if (!in.is_open())
    {
        std::cout << "Cannot open " << file << std::endl;
        exit(-1);
    }
    
    std::string line;
    while (std::getline(in, line))
    {
        std::string::size_type pos = line.find_first_of("#");
        line = line.substr(0, pos);
        pos_type dat;
        std::string word;
        std::stringstream ss(line);
        std::vector<double> values;
        
        while(ss >> word)
        {
            std::stringstream ss1(word);
            double val;
            ss1 >> val;
            values.push_back(val);
        }
        
        if (values.size() == 5)
        {
            dat.time = values[0];
            dat.x = values[1]*1000.0;
            dat.y = values[2]*1000.0;
            dat.xerr = values[3]*1000.0;
            dat.yerr = values[4]*1000.0;
        }
        else
        {
            std::cout << (file + ".points") << " file in unrecognized format" << std::endl;
            exit(-1);
        }
        
        pos_in.push_back(dat);
    }
    
    std::ifstream in2((file + ".rv").c_str());
    if (!in2.is_open())
    {
        std::cout << "Cannot open " << file << std::endl;
        exit(-1);
    }

    while (std::getline(in2, line))
    {
        std::string::size_type pos = line.find_first_of("#");
        line = line.substr(0, pos);
        vel_type dat;
        std::string word;
        std::stringstream ss(line);
        std::vector<double> values;
        
        while(ss >> word)
        {
            std::stringstream ss1(word);
            double val;
            ss1 >> val;
            values.push_back(val);
        }
        
        if (values.size() == 3)
        {
            dat.time = values[0];
            dat.velocity = values[1];
            dat.verr = values[2];
        }
        else
        {
            std::cout << (file + ".rv") << " file in unrecognized format" << std::endl;
            exit(-1);
        }
        
        vel_in.push_back(dat);
    }
}

#endif
