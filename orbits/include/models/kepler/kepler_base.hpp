//  EFIT 6
//  *********************************************
///  \file kepler_base.hpp
///
///  \brief The base class for orbit that are based on a 2D elliptic/hyperbolic-like shape.
///
///   In this header are the methods common for all models based on a 2D elliptic/hyperbolic orbit. 
///   This class is intented to perform the rotation between the 2D model and the observables.
///   The observables are (x,y) in milliarcseconds and RV in km/s. 
///   Conventions: x>0 is directed towards West (opposite to RA but same as camera pixel).
///                y>0 is directed towards North
///                Vz>0 means that the star is moving away from us.
///
///   It requires the following parameters:
///     -   global_tags = {"x0", "y0", "vx0", "vy0", "vz0", "R0"} where x0, y0, vx0, vy0 and vz0 are
///         the position/velocity of the central mass at time=tOrigin (see utils/include/constants.hpp). 
///         The units of x0 and y0 are milliarcsecond and vx0 and vy0 are in mas/yr while vz0 is in km/s.
///         The positive values of x0 and vx0 point towards EAST (opposite of observations).
///         The positive values of y0 and vy0 point towards NORTH. 
///         The positive values of vz0 mean that the SMBH is moving away from us.
///         R0 is the distance to the GC in kpc.
///          
///     - local_tags = {"omega", "Omega", "I"}: standard angles to parametrize the orbit orientation expressed in degrees.
///         The definition of the orbital parameter can be found in Section 3.3.1 of Ghez et al, 2005, ApJ 620, 744.
///
///
///  *********************************************
///
///  Authors (add name and date if you modify):
///
///  \author Gregory Martinez (gregory.david.martinez@gmail.com)
///          A. Hees (ahees@astro.ucla.edu)
///
///  \date 2016 May
///
///  *********************************************

#ifndef __KEPLER_BASE_HPP__
#define __KEPLER_BASE_HPP__

#include "constants.hpp"
#include "mean_anomaly.hpp"
#include <iostream>
#include <boost/range/join.hpp>
namespace efit
{
    
    namespace orbits
    {

        ///base class to everything orbity.  Its does a euler rotation based and requires
        
        class kepler_base : public orbits_base
        {
        private:
            double TI_A,TI_B,TI_C,TI_F,TI_G,TI_H;///< Thieles Innes constant (see ref?)
            double x0, y0;           ///< position of the central mass at tOrigin. Expressed in mas.
            double vx0, vy0;         ///< position of the central mass at tOrigin. Expressed in mas.
            double vz0;              ///< velocity of the central mass along the line of sight in km/s (positive value meaning the central mass is moving away from us)
            double cosO, sinO, coso, sino, cosI, sinI;  ///< cos and sin of \Omega, \omega and inclination.
            double  R;               ///< distnace to the GC in kpc.

            
        public:
            ///  \fn  kepler_base(const YAML::Node &node) 
            ///  \brief construtor. Set the tags parameters.
         		///
         		///
            kepler_base(const YAML::Node &node) 
            {
                AddGlobalTags({"x0", "y0", "vx0", "vy0", "vz0", "R0"});
                AddLocalTags({"omega", "Omega", "I"});
            }
            
        protected:
            ///  \fn  void InputParams(map_type<std::string, double> &global_map, map_type<std::string, double> &local_map)
            ///  \brief Function to be called before evaluating a model to set up the constants.
         		///
            ///  \param[in] global_map map containing the values of the global parameters
            ///  \param[in] local_map map containing the values of the local parameters
            ///  \return the different variables (x0, vx0, y0, vy0, vz0, R0, Omega, omega and I) are computed and initialized.
            ///
            void InputParams(map_type<std::string, double> &global_map, map_type<std::string, double> &local_map)
            {
                x0   = global_map["x0"];
                y0   = global_map["y0"];
                vx0  = global_map["vx0"]; 
                vy0  = global_map["vy0"]; 
                vz0  = global_map["vz0"];
                R    = global_map["R0"];

                cosO = std::cos(local_map["Omega"]*constants::deg_to_rad);
                sinO = std::sin(local_map["Omega"]*constants::deg_to_rad);
                coso = std::cos(local_map["omega"]*constants::deg_to_rad);
                sino = std::sin(local_map["omega"]*constants::deg_to_rad);
                cosI = std::cos(local_map["I"]*constants::deg_to_rad);
                sinI = std::sin(local_map["I"]*constants::deg_to_rad);
                
                // Thieles Innes constant
                TI_A =  cosO*coso - cosI*sinO*sino;
                TI_B =  sinO*coso + cosI*cosO*sino;
                TI_F = -cosO*sino - cosI*sinO*coso;
                TI_G = -sinO*sino + cosI*cosO*coso;
                TI_C = sinI*sino;
                TI_H = sinI*coso;

            }
            


            ///  \fn  double RotateX(double X, double Y) const
            ///  \brief transformation from the orbital plane (X,Y) to the x-coordinates 
            ///  (positive value of x directed towards the west, i.e. opposite to RA but in the same direction as camera coord.)
         		///
         		///  \param[in] X X-position of the stars in the orbital plane 
         		///  \param[in] Y Y-position of the stars in the orbital plane 
            ///  \return  x-position of the stars in the global reference frame in AU. The x axis points towards west like the camera coord.)
            ///
            double RotateX(double X, double Y) const {return -(TI_B*X + TI_G*Y);}
            
            
            ///  \fn  double RotateY(double X, double Y) const
            ///  \brief transformation from the orbital plane (X,Y) to the y-coordinates 
            ///  (positive value of y directed towards the North, i.e. like declination)
         		///
         		///  \param[in] X X-position of the stars in the orbital plane 
         		///  \param[in] Y Y-position of the stars in the orbital plane 
            ///  \return  y-position of the stars in the global reference frame in AU. The y axis points towards North)
            ///
            double RotateY(double X, double Y) const {return TI_A*X + TI_F*Y;}
            
            ///  \fn  double RotateZ(double X, double Y) const
            ///  \brief transformation from the orbital plane (X,Y) to the z-coordinates 
            ///  (positive value of z directed towards the GC, i.e. away from us)
         		///
         		///  \param[in] X X-position of the stars in the orbital plane 
         		///  \param[in] Y Y-position of the stars in the orbital plane 
            ///  \return  z-position of the stars in the global reference frame in AU. The z axis points towards the GC, i.e. away from us)
            ///
            double RotateZ(double X, double Y) const {return TI_C*X + TI_H*Y;}
            
            ///  \fn  double der_RotateX(double X, double Y,std::map<std::string, double> &dX, std::map<std::string, double> &dY, std::unordered_map<std::string, double> &dx)
            ///  \brief transformation from the orbital plane (X,Y) to the x-coordinates with transformation of partial derivatives
            ///  (positive value of x directed towards the west, i.e. opposite to RA but in the same direction as camera coord.)
         		///
         		///  \param[in] X X-position of the stars in the orbital plane 
         		///  \param[in] Y Y-position of the stars in the orbital plane
         		///  \param[in] dX map of derivatives of X with respect to the parameters.
         		///  \param[in] dY map of derivatives of Y with respect to the parameters.
         		///  \param[out] dx map of the derivatives of x with respect to the parameters.
            ///  \return  x-position of the stars in the global reference frame in AU. The x axis points towards west like the camera coord.). 
            ///           dx contains the derivatives of x with respect to the parameters.
            ///
            double der_RotateX(double X, double Y,std::map<std::string, double> &dX, std::map<std::string, double> &dY, std::unordered_map<std::string, double> &dx)
            {
              for (auto &&tup : zip(dX, dY))// rotation
              {
                  dx[tup.get<0>().first] = RotateX(tup.get<0>().second, tup.get<1>().second);
              }
              // addition for the rotation for the angles
              dx["Omega"]=dx["Omega"]-(TI_A*X + TI_F*Y);
              dx["omega"]=dx["omega"]-TI_G*X + TI_B*Y;
              dx["I"]    =dx["I"]+cosO*(TI_C*X + TI_H*Y);
              return RotateX(X,Y); 
            }
            
            
            ///  \fn  double der_RotateY(double X, double Y,std::map<std::string, double> &dX, std::map<std::string, double> &dY, std::unordered_map<std::string, double> &dy)
            ///  \brief transformation from the orbital plane (X,Y) to the y-coordinates with the transformation of the partial derivatives
            ///  (positive value of y directed towards the North, i.e. like declination)
         		///
         		///  \param[in] X X-position of the stars in the orbital plane 
         		///  \param[in] Y Y-position of the stars in the orbital plane
         		///  \param[in] dX map of derivatives of X with respect to the parameters.
         		///  \param[in] dY map of derivatives of Y with respect to the parameters.
         		///  \param[out] dy map of the derivatives of x with respect to the parameters.
            ///  \return  y-position of the stars in the global reference frame. The y axis points towards North like the camera coord.). 
            ///           dy contains the derivatives of y with respect to the parameters.
            ///
            double der_RotateY(double X, double Y,std::map<std::string, double> &dX, std::map<std::string, double> &dY, std::unordered_map<std::string, double> &dy)
             {
               for (auto &&tup : zip(dX, dY))// rotation
               {
                   dy[tup.get<0>().first] = RotateY(tup.get<0>().second, tup.get<1>().second);
               }
               // addition for the rotation for the angles
               dy["Omega"]=dy["Omega"]-(TI_B*X + TI_G*Y);
               dy["omega"]=dy["omega"]+TI_F*X - TI_A*Y;
               dy["I"]    =dy["I"]+sinO*(TI_C*X + TI_H*Y);
               return RotateY(X,Y);
             }

            ///  \fn  double der_RotateZ(double X, double Y,std::map<std::string, double> &dX, std::map<std::string, double> &dY, std::unordered_map<std::string, double> &dz)
            ///  \brief transformation from the orbital plane (X,Y) to the z-coordinates with the transformation of the partial derivatives
            ///  (positive value of z directed towards the GC, i.e. away form us).
         		///
         		///  \param[in] X X-position of the stars in the orbital plane 
         		///  \param[in] Y Y-position of the stars in the orbital plane
         		///  \param[in] dX map of derivatives of X with respect to the parameters.
         		///  \param[in] dY map of derivatives of Y with respect to the parameters.
         		///  \param[out] dz map of the derivatives of z with respect to the parameters.
            ///  \return  z-position of the stars in the global reference. The z axis points away from us. 
            ///           dz contains the derivatives of z with respect to the parameters.
            ///
            double der_RotateZ(double X, double Y,std::map<std::string, double> &dX, std::map<std::string, double> &dY, std::unordered_map<std::string, double> &dz)
              {
                for (auto &&tup : zip(dX, dY))// rotation
                {
                    dz[tup.get<0>().first] = RotateZ(tup.get<0>().second, tup.get<1>().second);
                }
                // addition for the rotation for the angles
                dz["omega"]=dz["omega"]+TI_H*X - TI_C*Y;
                dz["I"]    =dz["I"]+cosI*(sino*X + coso*Y);
                return RotateZ(X,Y);
             }
             
            ///  \fn  TranslateX(double X, double t)
            ///  \brief transformation from the X coordinates in AU expressed in a GC reference frame to camera observables x 
            ///   expressed in milliarcsecond. 
    		    ///
    		    ///  \param[in] X X-position of the stars in the GC reference frame in AU
    		    ///  \param[in] t time of the observations (in Julian years)
            ///  \return  x position in camera coordinates in mas. Warning: the position and velocity of the SMBH (x0 and vx0) 
            ///   are given in the East direction while positive values for x are in the West direction (this explains the - sign in this method)
            /// 
            double TranslateX(double X, double t) const {return X/R - x0 - vx0*(t - constants::tOrigin);}
       
            ///  \fn  TranslateY(double Y, double t)
            ///  \brief transformation from the Y coordinates in AU expressed in a GC reference frame to camera observables y 
            ///   expressed in milliarcsecond. 
    		    ///
    		    ///  \param[in] Y Y-position of the stars in the GC reference frame in AU
    		    ///  \param[in] t time of the observations (in Julian years)
            ///  \return  y position in camera coordinates in mas (positive value directed towards North).
            ///
            double TranslateY(double Y, double t) const {return Y/R + y0 + vy0*(t - constants::tOrigin);}

            ///  \fn  TranslateVz(double VZ)
            ///  \brief transformation from the Vz velocity in AU/yr expressed in a GC reference frame to RV measurement
            ///         in km/s (transformation of units + shift with BH velocity)
    		    ///
    		    ///  \param[in] VZ VZ velocity of the star in the GC reference frame in AU/yr (positive value pointing away from us)
            ///  \return  velocity of the stars in km/s corrected with the BH velocity.
            ///
            double TranslateVz(double VZ) const {return constants::auyr_to_kms*VZ + vz0;}  
        
        
            ///  \fn  double der_TranslateX(double X, double t, std::unordered_map<std::string, double> &dx)
            ///  \brief transformation from the X coordinates in AU expressed in a GC reference frame to camera observables x 
            ///   expressed in milliarcsecond + transformation of the partial derivatives.
    		    ///
    		    ///  \param[in] X X-position of the stars in the GC reference frame in AU
    		    ///  \param[in] t time of the observations (in Julian years)
    		    ///  \param   dx a map of the partial derivatives. At input, partial derivatives of X wrt parameters and at output, partial derivatives of x wrt parameters.
            ///  \return  x position in camera coordinates in mas. Warning: the position and velocity of the SMBH (x0 and vx0) 
            ///   are given in the East direction while positive values for x are in the West direction (this explains the - sign in this method). The dx map is updated.
            ///        
            double der_TranslateX(double X, double t, std::unordered_map<std::string, double> &dx)
            {
              for (auto && key : GlobalTags())
                {
                  dx[key]/=R;
                }
              for (auto && key : LocalTags())
              {
                dx[key]/=R;
              }
              //der wrt to R0, vx0, x0
              dx["R0"]-=X/R/R;
              dx["x0"]-=1.;
              dx["vx0"]-=t - constants::tOrigin;
             
              return TranslateX(X,t);
            }
            
     
            ///  \fn  double der_TranslateY(double Y, double t, std::unordered_map<std::string, double> &dx)
            ///  \brief transformation from the Y coordinates in AU expressed in a GC reference frame to camera observables y 
            ///   expressed in milliarcsecond + transformation of the partial derivatives.
    		    ///
    		    ///  \param[in] Y Y-position of the stars in the GC reference frame in AU
    		    ///  \param[in] t time of the observations (in Julian years)
    		    ///  \param   dy a map of the partial derivatives. At input, partial derivatives of Y wrt parameters and at output, partial derivatives of y wrt parameters.
            ///  \return  y position in camera coordinates in mas.  The dy map is updated.
            ///            
            double der_TranslateY(double Y, double t, std::unordered_map<std::string, double> &dy)
            {
              for (auto && key : GlobalTags())
                {
                  dy[key]/=R;
                }
              for (auto && key : LocalTags())
              {
                dy[key]/=R;
              }
              //der wrt to R0, vx0, x0
              dy["R0"]-=Y/R/R;
              dy["y0"]+=1.;
              dy["vy0"]+=t - constants::tOrigin;
               
              return TranslateY(Y,t);
            }
            
            
            ///  \fn  double der_TranslateVz(double VZ, std::unordered_map<std::string, double> &dv)
            ///  \brief transformation from the Vz velocity in AU/yr expressed in a GC reference frame to RV measurement
            ///         in km/s (transformation of units + shift with BH velocity)
    		    ///
    		    ///  \param[in] VZ VZ velocity of the star in the GC reference frame in AU/yr (positive value pointing away from us)
    		    ///  \param[in] t time of the observations (in Julian years)
    		    ///  \param   dv a map of the partial derivatives. At input, partial derivatives of VZ wrt parameters and at output, partial derivatives of vz wrt parameters.
            ///  \return  velocity of the stars in km/s corrected with the BH velocity + the map dv has been updated.
            ///            
            double der_TranslateVz(double VZ, std::unordered_map<std::string, double> &dv)
            {
              for (auto && key : GlobalTags())
                {
                  dv[key]*=constants::auyr_to_kms;
                }
              for (auto && key : LocalTags())
              {
                dv[key]*=constants::auyr_to_kms;
              }
              dv["vz0"]+=1.;
               
              return TranslateVz(VZ);
            }
            
            ///  \fn  void der_rad2deg(std::unordered_map<std::string, double> &der)
            ///  \brief transformation of the partials derivatives with respect to the angles from radians to degrees.
    		    ///
    		    ///  \param der map of partial derivatives
            ///  \return  the map der is updated such that the partials with respect to angular variables are transformed from rad to deg.
            ///            
            void der_rad2deg(std::unordered_map<std::string, double> &der)
            {
              der["Omega"]*=M_PI/180.;
              der["omega"]*=M_PI/180.;
              der["I"]*=M_PI/180.;              
            }

            
            virtual ~kepler_base(){}
        };
        
    }
    
}

#endif
