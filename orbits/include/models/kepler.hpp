//  EFIT 6
//  *********************************************
///  \file kepler.hpp
///
///  \brief This file contains the kepler model.
///
///   In this header are the methods used to implement a Kepler orbital model. More precisely, the following classes
///   inherit from kepler_base.hpp where the rotation from the 2D orbital plane and the observables is performed. These classes
///   are supposed to compute the 2D orbital model.
///
///
///  *********************************************
///
///  Authors (add name and date if you modify):
///
///  \author Gregory Martinez (gregory.david.martinez@gmail.com)
///          A. Hees (ahees@astro.ucla.edu)
///
///  \date 2016 May
///
///  *********************************************
#ifndef __KEPLER_HPP__
#define __KEPLER_HPP__

#include "constants.hpp"
#include "mean_anomaly.hpp"
#include "models/kepler/kepler_base.hpp"
#include <iostream>
#include <iomanip>
namespace efit
{    
    namespace orbits
    {
        ///  \class kepler_abstract
        ///  \brief Intermediate class for a Kepler orbital model. 
        ///
        ///
        class kepler_abstract : public kepler_base
        {
        protected:
            double t;
            
        public:
            ///  \fn  kepler_abstract(const YAML::Node &node) 
            ///  \brief construtor. (needs to be overloaded)
         		///
            kepler_abstract(const YAML::Node &node) : kepler_base(node)
            {
            }
            
            ///  \fn  void operator()(map_type<std::string, double> &global_map, map_type<std::string, double> &local_map, vec_type<position_type> &pos_obs, vec_type<velocity_type> &vel_obs)
            ///  \brief main operator that returns the simulated observables (astro and RV) for certain given time.
            ///
            ///
         		///  \param[in] global_map : map that contains the global parameters for which we want to evaluate the model
         		///  \param[in] local_map : map that contains the local parameters (for one given star) for which we want to evaluate the model
         		///  \param  pos_obs : pointer toward a vector of element of type: position_type (see orbits_interface.hpp for this typedef). 
         		///          The time component of the elements of the vector represents the date where we want to simulate astrometric observations (in Julian years).
         		///  \param  vel_obs : pointer toward a vector of element of type: velocity_type (see orbits_interface.hpp for this typedef). 
         		///          The time component of the elements of the vector represents the date where we want to simulate RV observations (in Julian years).
         		///
         		///   \return  pos_obs and vel_obs are updated with the astro/RV values corresponding to the model.
         		///
            void operator()(map_type<std::string, double> &global_map, map_type<std::string, double> &local_map, vec_type<position_type> &pos_obs, vec_type<velocity_type> &vel_obs)
            {
                kepler_base::InputParams(global_map, local_map);
                InputParams(global_map,local_map);
                
                for (auto &&pos : pos_obs)// loop for astrometric observations
                {
                    EnterTime(pos.time);
                    double x = X();
                    double y = Y();
                    pos.x = TranslateX(RotateX(x, y), t);
                    pos.y = TranslateY(RotateY(x, y), t);
                }
                
                for (auto &&vel: vel_obs)// loop for RV observations
                {
                    EnterTime(vel.time);
                    vel.velocity = TranslateVz(RotateZ(VX(), VY()));
                }
            }
            
            ///  \fn  void operator()(map_type<std::string, double> &global_map, map_type<std::string, double> &local_map, vec_type<dposition_type> &pos_obs, vec_type<dvelocity_type> &vel_obs)
            ///  \brief main operator that returns the simulated observables (astro and RV) and their partial derivatives for certain given time.
            ///
            ///
         		///  \param[in] global_map : map that contains the global parameters for which we want to evaluate the model
         		///  \param[in] local_map : map that contains the local parameters (for one given star) for which we want to evaluate the model
         		///  \param  pos_obs : pointer toward a vector of element of type: dposition_type (see orbits_interface.hpp for this typedef). 
         		///          The time component of the elements of the vector represents the date where we want to simulate astrometric observations (in Julian years).
         		///  \param  vel_obs : pointer toward a vector of element of type: dvelocity_type (see orbits_interface.hpp for this typedef). 
         		///          The time component of the elements of the vector represents the date where we want to simulate RV observations (in Julian years).
         		///
         		///   \return  pos_obs and vel_obs are updated with the astro/RV values corresponding to the model + the partials derivative.
         		///
            void operator()(map_type<std::string, double> &global_map, map_type<std::string, double> &local_map, vec_type<dposition_type> &pos_obs, vec_type<dvelocity_type> &vel_obs)
            {
                kepler_base::InputParams(global_map, local_map);
                InputParams(global_map,local_map);
                
                static std::map<std::string, double> dXs=initDer();
                static std::map<std::string, double> dYs=initDer();
                static std::map<std::string, double> dVxs=initDer();
                static std::map<std::string, double> dVys=initDer();
                
                for (auto &&pos : pos_obs)// loop for astrometric observations
                {
                    EnterTime(pos.time);
                    
                    // value of quantity in the orbital plane reference frame
                    double x = X();
                    double y = Y();
                    dPos(dXs, dYs);
                    
                    // rotation
                    pos.x=der_RotateX(x,y,dXs, dYs, pos.dx);
                    pos.y=der_RotateY(x,y,dXs, dYs, pos.dy);

                    // translation
                    pos.x=der_TranslateX(pos.x,t,pos.dx);
                    pos.y=der_TranslateY(pos.y,t,pos.dy);
                    
                    // rad 2 deg for partials
                    der_rad2deg(pos.dx);
                    der_rad2deg(pos.dy);
                }
                
                for (auto &&vel: vel_obs)// loop for RV observations
                {
                    EnterTime(vel.time);
                    // value of quantity in the orbital plane reference frame
                    double vx = VX();
                    double vy = VY();
                    dVel(dVxs, dVys);
  
                    // rotation
                    vel.velocity=der_RotateZ(vx,vy,dVxs, dVys, vel.dvelocity);

                    //translation
                    vel.velocity = der_TranslateVz(vel.velocity,vel.dvelocity);
                    der_rad2deg(vel.dvelocity);
                }
            }
            
            virtual double X() const = 0;
            virtual double Y() const = 0;
            virtual double VX() const = 0;
            virtual double VY() const = 0;
            virtual void EnterTime(double) = 0;
            virtual void dPos(std::map<std::string, double> &, std::map<std::string, double> &) const = 0;
            virtual void dVel(std::map<std::string, double> &, std::map<std::string, double> &) const = 0;
            virtual void InputParams(map_type<std::string, double> &,map_type<std::string, double> &) = 0;
        };
        
        ///  \class kepler
        ///  \brief Class that inherits orbit_base via kepler_base. Use to describe an orbital model based on a Kepler ellipse.
        ///   The global tag (in addition to thoses from the parents classes) is "Mass"
        ///   The local tags (in addition to thoses from the parents classes) is {"P","e","T0"}. P is the orbital period in Julian
        ///   years, e the eccentricity and T0 the time of closest approach in J. years.
        ///
        ///
        class kepler : public kepler_abstract
        {
        private:
            double P;           ///< period in Julian years
            double meanMotion;  ///< mean motion = 2pi/P in Julian years^-1
            double T0;          ///< time of closest approach in Julian years
            double e;           ///< eccentricity
            double a;           ///< semi major axis in AU
            double b;           ///< semi-minor axis in AU
            double E;           ///< eccentric anomaly in radians
            double cosE;        ///< cos of eccentric anomaly 
            double sinE;        ///< sin of eccentric anomaly

            double mass;        ///< central mass (SMBH) in 10^6 Sun mass.
            
        public:
            ///  \fn  kepler(const YAML::Node &node) 
            ///  \brief construtor. It sets the tags (global: "Mass", local: {"P","e","T0"})
           	///
            kepler(const YAML::Node &node) : kepler_abstract(node)
            {
                AddLocalTags({"P", "e", "T0"});
                AddGlobalTags({"Mass"});
            }
            
            ///  \fn  void InputParams(std::unordered_map<std::string, double> &global_map,std::unordered_map<std::string, double> &local_map)
            ///  \brief Function to be called before evaluating a model to set up the constants. 
            ///
            ///  \param[in] global_map map containing the values of the global parameters
            ///  \param[in] local_map map containing the values of the local parameters
            ///  \return the different variables (P, e, T0, mass, a, b, meanMotion) are computed and initialized.
            ///
            void InputParams(std::unordered_map<std::string, double> &global_map,std::unordered_map<std::string, double> &local_map)
            {
//                a = local_map["a"];
//                P = std::sqrt(a*a*a/Mass()/constants::MGM_sun_4pi2);
                P = local_map["P"];
                e = local_map["e"];
                T0 = local_map["T0"];
                mass = global_map["Mass"];
                
                a = pow(P*P*mass*constants::MGM_sun_4pi2,1./3.);
                b = a*std::sqrt(1.0 - e*e);
                meanMotion = constants::_2pi/P;
            }
            
            ///  \fn  void EnterTime(double tin)
            ///  \brief Function to be called to compute the eccentric anomaly.
            ///
            ///  \param[in] tin time where one wants to evaluate the model in Julian years.
            ///  \return the eccentric anomaly is computed and the variables E, cosE and sinE are initialized.
            ///
            void EnterTime(double tin) 
            {
                t = tin;
                E = BoundMeanAnomaly(e, meanMotion*(t - T0));
                cosE = std::cos(E);
                sinE = std::sin(E);
            }
            
            ///  \fn  double X() const
            ///  \brief Return the X position in AU in the orbital reference frame.
            ///
            /// \pre methods InputParams and EnterTime needs to be called previously.
            ///  \return the X position of the star in AU in the orbital reference frame.
            ///
            double X() const {return a*(cosE - e);}
            
            ///  \fn  double Y() const
            ///  \brief Return the Y position in AU in the orbital reference frame.
            ///
            /// \pre methods InputParams and EnterTime needs to be called previously.
            ///  \return the Y position of the stars in AU in the orbital reference frame.
            ///
            double Y() const {return b*sinE;}
            
            ///  \fn  double VX() const
            ///  \brief Return the X velocity in AU/yr in the orbital reference frame.
            ///
            /// \pre methods InputParams and EnterTime needs to be called previously.
            ///  \return the X velocity of the star in AU/yr in the orbital reference frame.
            ///
            double VX() const {return -a*meanMotion*sinE/(1.0 - e*cosE);}
            
            ///  \fn  double VY() const
            ///  \brief Return the Y velocity in AU/yr in the orbital reference frame.
            ///
            /// \pre methods InputParams and EnterTime needs to be called previously.
            ///  \return the Y velocity of the star in AU/yr in the orbital reference frame.
            ///
            double VY() const {return b*meanMotion*cosE/(1.0 - e*cosE);}
            
            
            ///  \fn  void dPos(std::map<std::string, double> &Xs, std::map<std::string, double> &Ys) const
            ///  \brief Function to compute the partial derivatives of the positions (in the orbital reference frame)
            ///   with respect to the parameters related to this class (see constructor: here {"Mass","P","e","T0"})
            ///
            /// \pre methods InputParams and EnterTime needs to be called previously.
            ///  \param Xs map that contains (as an output) the partials of X with respect to the parameters related to this class
            ///  \param Ys map that contains (as an output) the partials of Y with respect to the parameters related to this class
            ///
            void dPos(std::map<std::string, double> &Xs, std::map<std::string, double> &Ys) const
            {
                double dom = 1.0 - e*cosE;
                double dx = -a*sinE/dom;
                double dy = b*cosE/dom;
                double m = E - e*sinE;

                Xs["P"]    = 2./3.*a/P*(cosE-e) - m*dx/P;
                Xs["e"]    = -a*(2.0 - (cosE + e)*cosE)/dom;
                Xs["T0"]   = -dx*meanMotion;
                Xs["Mass"] = a*(cosE-e)/3./mass;

                Ys["P"]    = 2./3.*b*sinE/P - m*dy/P;
                Ys["Mass"] = b*sinE/3./mass;
                Ys["e"]    = b*sinE*(cosE - e)/(1.0 - e*e)/dom;
                Ys["T0"]   = -dy*meanMotion;
            }
            
            
            ///  \fn  void dVel(std::map<std::string, double> &Vxs, std::map<std::string, double> &Vys) const
            ///  \brief Function to compute the partial derivatives of the velocities (in the orbital reference frame)
            ///   with respect to the parameters related to this class (see constructor: here {"Mass","P","e","T0"})
            ///
            /// \pre methods InputParams and EnterTime needs to be called previously.
            ///  \param Vxs map that contains (as an output) the partials of Vx with respect to the parameters related to this class
            ///  \param Vys map that contains (as an output) the partials of Vy with respect to the parameters related to this class
            ///
            void dVel(std::map<std::string, double> &Vxs, std::map<std::string, double> &Vys) const
            {
                double dom = 1.0 - e*cosE;
                double dvx = -a*meanMotion*(cosE - e)/dom/dom/dom;
                double dvy = -b*meanMotion*sinE/dom/dom/dom;
                double m = E - e*sinE;

                Vxs["P"]    = 1./3.*a*meanMotion*sinE/P/dom  - m*dvx/P;
                Vxs["e"]    = -a*meanMotion*sinE*((2.0 - e*cosE)*cosE - e)/dom/dom/dom;
                Vxs["T0"]   = -dvx*meanMotion;
                Vxs["Mass"] = -a*meanMotion*sinE/(dom*3.*mass);                
                
                Vys["P"]    = -1./3.*b*meanMotion*cosE/P/dom - m*dvy/P;
                Vys["e"]    = b*meanMotion*((cosE*cosE*(2.0 - e*cosE)-e*cosE)/(1.-e*e) - 1.0)/dom/dom/dom;
                Vys["T0"]   = -dvy*meanMotion;
                Vys["Mass"] = b*meanMotion*cosE/(dom*3.*mass);

            }
        };




        ///  \class general_orbit
        ///  \brief Class that inherits orbit_base via kepler_abstract and kepler_base. Use to describe an orbital model based on 
        ///    a Kepler model: ellipse or hyperbolic orbits.
        ///   The global tag (in addition to thoses from the parents classes) is "Mass"
        ///   The local tags (in addition to thoses from the parents classes) is {"a","e","T0"}. What is a exactly??? , e the eccentricity and T0 the time of closest approach in J. years.
        ///
        ///  \warning NOT TESTED (NOT EVEN COMPLETELY FINISHED)
        ///
        class general_orbit : public kepler_abstract
        {
        private:
            double alpha, e, T0, l, costheta, sintheta, sqrtem1, r, dt,mass;
            
        public:
            general_orbit(const YAML::Node &node) : kepler_abstract(node)
            {
              AddLocalTags({"a", "e", "T0"});
              AddGlobalTags({"Mass"});
            }
            
            void InputParams(std::unordered_map<std::string, double> &global_map,std::unordered_map<std::string, double> &local_map)
            {
              alpha = local_map["a"];
              e = local_map["e"];
              T0 = local_map["T0"];
              mass = global_map["Mass"];
                
                sqrtem1 = std::sqrt(std::abs(1.0 - e*e));
                l = constants::_2pi*std::sqrt(alpha*mass*constants::MGM_sun_4pi2);
            }
            
            double X() const {return r*costheta;}
            double Y() const {return r*sintheta;}
            double VX() const {return -alpha*l*sintheta/SQ(1.0 + e*costheta)/r/r;}
            double VY() const {return alpha*l*(costheta + e)/SQ(1.0 + e*costheta)/r/r;}
            
            void dPos(std::map<std::string, double> &Xs, std::map<std::string, double> &Ys) const
            {
            }
            
            void dVel(std::map<std::string, double> &Vxs, std::map<std::string, double> &Vys) const
            {
            }
            
            void EnterTime(double tin)
            {
                t = tin;
                double theta;
                
                if (e < 1.0)
                {
                    double a = alpha/(1.0 - e*e);
                    double b = a*sqrtem1;
                    
                    double E = BoundMeanAnomaly(e, l/a/b*(t-T0));
                    theta = 2.0*std::atan(std::tan(E/2.0)*std::sqrt((1.0+e)/(1.0-e)));
                }
                else if (e > 1.0)
                {
                    double a = alpha/(e*e - 1.0);
                    double b = a*sqrtem1;
                    
                    double E = UnBoundMeanAnomaly(e, l/a/b*(t-T0));
                    theta = 2.0*std::atan(std::tanh(E/2.0)*std::sqrt((1.0+e)/(e-1.0)));
                }
                else if (e == 1.0)
                {
                    theta = 2.0*std::atan(EqBoundMeanAnomaly(l*(t - T0)/SQ(alpha)));
                }
                
                costheta = std::cos(theta);
                sintheta = std::sin(theta);
                r = alpha/(1.0 + e*costheta);
            }
        };
        
        LOAD_ORBITS("kepler", kepler)
        LOAD_ORBITS("unbound", general_orbit)
        
    }
    
}

#endif
