//  EFIT 6
//  *********************************************
///  \file keplerRedshift.hpp
///
///  \brief This file contains the kepler model corrected with the Romer time delay and the relativistic redshift. THis model is analytical.
///
///   In this header are the methods used to implement a Kepler orbital model corrected with the Romer time delay and the relativistic redshift.
///  More precisely, the following classes  inherit from kepler_base.hpp where the rotation from the 2D orbital plane and the observables is performed. 
///  The class keplerRedsihft computes the 2D orbital model.
///  The corresponding tags are: global:{"Mass","Redhisft"} and local:{"P,"e", "T0}
///
/// \warning ROMER TIME DELAY NOT YET IMPLEMENTED !
///  *********************************************
///
///  Authors (add name and date if you modify):
///
///  \author A. Hees (ahees@astro.ucla.edu)
///          Gregory Martinez (gregory.david.martinez@gmail.com)
///          
///
///  \date 2016 May
///
///  *********************************************
#ifndef __KEPLERREDSHIFT_HPP__
#define __KEPLERREDSHIFT_HPP__

#include "constants.hpp"
#include "mean_anomaly.hpp"
#include "models/kepler/kepler_base.hpp"
#include <iostream>
#include <iomanip>
namespace efit
{    
    namespace orbits
    {
        ///  \class keplerRedhisft
        ///  \brief Class that implements a Kepler orbital model with corrections to take into account the Romer time delay
        ///         and the relativistic redshift. 
        ///
        ///
        class keplerRedshift : public kepler_base
        {
          private:
              double P;           ///< period in Julian years
              double meanMotion;  ///< mean motion = 2pi/P in Julian years^-1
              double T0;          ///< time of closest approach in Julian years
              double e;           ///< eccentricity
              double a;           ///< semi major axis in AU
              double b;           ///< semi-minor axis in AU
              double E;           ///< eccentric anomaly in radians
              double cosE;        ///< cos of eccentric anomaly 
              double sinE;        ///< sin of eccentric anomaly
              double relRedshift;    ///< parameter that parametrizes the relativstic redhisft (1=GR, 0=no relativistic redshift)

              double mass;        ///< central mass (SMBH) in 10^6 Sun mass.
              double t;
              

                  
        public:
            ///  \fn  keplerRedshift(const YAML::Node &node) 
            ///  \brief construtor. It sets the tags (global: "Mass", "Redshift", local: {"P","e","T0"})
           	///
            keplerRedshift(const YAML::Node &node) : kepler_base(node)
            {

                AddLocalTags({"P", "e", "T0"});
                AddGlobalTags({"Mass","Redshift"});
            }

            ///  \fn  void InputParams(std::unordered_map<std::string, double> &global_map,std::unordered_map<std::string, double> &local_map)
            ///  \brief Function to be called before evaluating a model to set up the constants. 
            ///
            ///  \param[in] global_map map containing the values of the global parameters
            ///  \param[in] local_map map containing the values of the local parameters
            ///  \return the different variables (P, e, T0, mass, a, b, meanMotion, redshift) are computed and initialized.
            ///
             void InputParams(std::unordered_map<std::string, double> &global_map,std::unordered_map<std::string, double> &local_map)
              {
                P = local_map["P"];
                e = local_map["e"];
                T0 = local_map["T0"];
                mass = global_map["Mass"];
                relRedshift = global_map["Redshift"];

                a = pow(P*P*mass*constants::MGM_sun_4pi2,1./3.);
                b = a*std::sqrt(1.0 - e*e);
                meanMotion = constants::_2pi/P;
              }
                          
            ///  \fn  void operator()(map_type<std::string, double> &global_map, map_type<std::string, double> &local_map, vec_type<position_type> &pos_obs, vec_type<velocity_type> &vel_obs)
            ///  \brief main operator that returns the simulated observables (astro and RV) for certain given time.
            ///
            ///
         		///  \param[in] global_map : map that contains the global parameters for which we want to evaluate the model
         		///  \param[in] local_map : map that contains the local parameters (for one given star) for which we want to evaluate the model
         		///  \param  pos_obs : pointer toward a vector of element of type: position_type (see orbits_interface.hpp for this typedef). 
         		///          The time component of the elements of the vector represents the date where we want to simulate astrometric observations (in Julian years).
         		///  \param  vel_obs : pointer toward a vector of element of type: velocity_type (see orbits_interface.hpp for this typedef). 
         		///          The time component of the elements of the vector represents the date where we want to simulate RV observations (in Julian years).
         		///
         		///   \return  pos_obs and vel_obs are updated with the astro/RV values corresponding to the model.
         		///
            void operator()(map_type<std::string, double> &global_map, map_type<std::string, double> &local_map, vec_type<position_type> &pos_obs, vec_type<velocity_type> &vel_obs)
            {
                kepler_base::InputParams(global_map, local_map);
                InputParams(global_map,local_map);
                
                for (auto &&pos : pos_obs)// loop for astrometric observations
                {
                    EnterTime(pos.time);
                    double x = X();
                    double y = Y();
                    pos.x = TranslateX(RotateX(x, y), t);
                    pos.y = TranslateY(RotateY(x, y), t);
                }
                
                for (auto &&vel: vel_obs)// loop for RV observations
                {
                    EnterTime(vel.time);
                    double x = X();
                    double y = Y();
                    double Vx = VX();
                    double Vy = VY();
                    
                    vel.velocity = TranslateVz(RotateZ(Vx, Vy));
                    vel.velocity += redshift( x,  y,  Vx,  Vy);
                }
            }
            
            ///  \fn  void operator()(map_type<std::string, double> &global_map, map_type<std::string, double> &local_map, vec_type<dposition_type> &pos_obs, vec_type<dvelocity_type> &vel_obs)
            ///  \brief main operator that returns the simulated observables (astro and RV) and their partial derivatives for certain given time.
            ///
            ///
         		///  \param[in] global_map : map that contains the global parameters for which we want to evaluate the model
         		///  \param[in] local_map : map that contains the local parameters (for one given star) for which we want to evaluate the model
         		///  \param  pos_obs : pointer toward a vector of element of type: dposition_type (see orbits_interface.hpp for this typedef). 
         		///          The time component of the elements of the vector represents the date where we want to simulate astrometric observations (in Julian years).
         		///  \param  vel_obs : pointer toward a vector of element of type: dvelocity_type (see orbits_interface.hpp for this typedef). 
         		///          The time component of the elements of the vector represents the date where we want to simulate RV observations (in Julian years).
         		///
         		///   \return  pos_obs and vel_obs are updated with the astro/RV values corresponding to the model + the partials derivative.
         		///
            void operator()(map_type<std::string, double> &global_map, map_type<std::string, double> &local_map, vec_type<dposition_type> &pos_obs, vec_type<dvelocity_type> &vel_obs)
            {
                kepler_base::InputParams(global_map, local_map);
                InputParams(global_map,local_map);
                
                static std::map<std::string, double> dXs=initDer();
                static std::map<std::string, double> dYs=initDer();
                static std::map<std::string, double> dVxs=initDer();
                static std::map<std::string, double> dVys=initDer();
                
                for (auto &&pos : pos_obs)// loop for astrometric observations
                {
                    EnterTime(pos.time);
                    
                    // value of quantity in the orbital plane reference frame
                    double x = X();
                    double y = Y();
                    dPos(dXs, dYs);
                    
                    // rotation
                    pos.x=der_RotateX(x,y,dXs, dYs, pos.dx);
                    pos.y=der_RotateY(x,y,dXs, dYs, pos.dy);

                    // translation
                    pos.x=der_TranslateX(pos.x,t,pos.dx);
                    pos.y=der_TranslateY(pos.y,t,pos.dy);
                    
                    // rad 2 deg for partials
                    der_rad2deg(pos.dx);
                    der_rad2deg(pos.dy);
                }
                
                for (auto &&vel: vel_obs)// loop for RV observations
                {
                    EnterTime(vel.time);
                    // value of quantity in the orbital plane reference frame
                    double x  =  X();
                    double y  =  Y();
                    double vx = VX();
                    double vy = VY();
                    dPos(dXs, dYs);
                    dVel(dVxs, dVys);
  
                    // rotation
                    vel.velocity=der_RotateZ(vx,vy,dVxs, dVys, vel.dvelocity);

                    //translation
                    vel.velocity = der_TranslateVz(vel.velocity,vel.dvelocity);

                    // relativistic redshift
                    vel.velocity+=der_redshift(x,y,vx,vy, dXs,dYs,dVxs,dVys,vel.dvelocity);
                    der_rad2deg(vel.dvelocity);

                }
            }
            
            ///  \fn  void EnterTime(double tin)
            ///  \brief Function to be called to compute the eccentric anomaly.
            ///
            ///  \param[in] tin time where one wants to evaluate the model in Julian years.
            ///  \return the eccentric anomaly is computed and the variables E, cosE and sinE are initialized.
            ///
            void EnterTime(double tin) 
            {
                t = tin;
                E = BoundMeanAnomaly(e, meanMotion*(t - T0));
                cosE = std::cos(E);
                sinE = std::sin(E);
            }
            
            ///  \fn  double X() const
            ///  \brief Return the X position in AU in the orbital reference frame.
            ///
            /// \pre methods InputParams and EnterTime needs to be called previously.
            ///  \return the X position of the star in AU in the orbital reference frame.
            ///
            double X() const {return a*(cosE - e);}
            
            
            ///  \fn  double Y() const
            ///  \brief Return the Y position in AU in the orbital reference frame.
            ///
            /// \pre methods InputParams and EnterTime needs to be called previously.
            ///  \return the Y position of the stars in AU in the orbital reference frame.
            ///
            double Y() const {return b*sinE;}
            ///  \fn  double VX() const
            ///  \brief Return the X velocity in AU/yr in the orbital reference frame.
            ///
            /// \pre methods InputParams and EnterTime needs to be called previously.
            ///  \return the X velocity of the star in AU/yr in the orbital reference frame.
            ///
            double VX() const {return -a*meanMotion*sinE/(1.0 - e*cosE);}
            
            ///  \fn  double VY() const
            ///  \brief Return the Y velocity in AU/yr in the orbital reference frame.
            ///
            /// \pre methods InputParams and EnterTime needs to be called previously.
            ///  \return the Y velocity of the star in AU/yr in the orbital reference frame.
            ///
            double VY() const {return b*meanMotion*cosE/(1.0 - e*cosE);}
            
            
            ///  \fn  void dPos(std::map<std::string, double> &Xs, std::map<std::string, double> &Ys) const
            ///  \brief Function to compute the partial derivatives of the positions (in the orbital reference frame)
            ///   with respect to the parameters related to this class (see constructor: here {"Mass","P","e","T0"})
            ///
            /// \pre methods InputParams and EnterTime needs to be called previously.
            ///  \param Xs map that contains (as an output) the partials of X with respect to the parameters related to this class
            ///  \param Ys map that contains (as an output) the partials of Y with respect to the parameters related to this class
            ///
            void dPos(std::map<std::string, double> &Xs, std::map<std::string, double> &Ys) const
            {
                double dom = 1.0 - e*cosE;
                double dx = -a*sinE/dom;
                double dy = b*cosE/dom;
                double m = E - e*sinE;

                Xs["P"]    = 2./3.*a/P*(cosE-e) - m*dx/P;
                Xs["e"]    = -a*(2.0 - (cosE + e)*cosE)/dom;
                Xs["T0"]   = -dx*meanMotion;
                Xs["Mass"] = a*(cosE-e)/3./mass;

                Ys["P"]    = 2./3.*b*sinE/P - m*dy/P;
                Ys["Mass"] = b*sinE/3./mass;
                Ys["e"]    = b*sinE*(cosE - e)/(1.0 - e*e)/dom;
                Ys["T0"]   = -dy*meanMotion;
            }
            
            
            ///  \fn  void dVel(std::map<std::string, double> &Vxs, std::map<std::string, double> &Vys) const
            ///  \brief Function to compute the partial derivatives of the velocities (in the orbital reference frame)
            ///   with respect to the parameters related to this class (see constructor: here {"Mass","P","e","T0"})
            ///
            /// \pre methods InputParams and EnterTime needs to be called previously.
            ///  \param Vxs map that contains (as an output) the partials of Vx with respect to the parameters related to this class
            ///  \param Vys map that contains (as an output) the partials of Vy with respect to the parameters related to this class
            ///
            void dVel(std::map<std::string, double> &Vxs, std::map<std::string, double> &Vys) const
            {
                double dom = 1.0 - e*cosE;
                double dvx = -a*meanMotion*(cosE - e)/dom/dom/dom;
                double dvy = -b*meanMotion*sinE/dom/dom/dom;
                double m = E - e*sinE;

                Vxs["P"]    = 1./3.*a*meanMotion*sinE/P/dom  - m*dvx/P;
                Vxs["e"]    = -a*meanMotion*sinE*((2.0 - e*cosE)*cosE - e)/dom/dom/dom;
                Vxs["T0"]   = -dvx*meanMotion;
                Vxs["Mass"] = -a*meanMotion*sinE/(dom*3.*mass);                
                
                Vys["P"]    = -1./3.*b*meanMotion*cosE/P/dom - m*dvy/P;
                Vys["e"]    = b*meanMotion*((cosE*cosE*(2.0 - e*cosE)-e*cosE)/(1.-e*e) - 1.0)/dom/dom/dom;
                Vys["T0"]   = -dvy*meanMotion;
                Vys["Mass"] = b*meanMotion*cosE/(dom*3.*mass);

            }     
            
            double redshift(double X, double Y, double VX, double VY) const
             {
               return relRedshift*(0.5*(VX*VX+VY*VY)  +  mass*constants::MGM_sun/std::sqrt(X*X+Y*Y))*constants::auyr_to_kms/constants::c;
             }
             
             double der_redshift(double X, double Y, double VX, double VY,std::map<std::string, double> &dX,std::map<std::string, double> &dY,std::map<std::string, double> &dVx,std::map<std::string, double> &dVy,std::unordered_map<std::string, double> &drv) const
              {
                double r=std::sqrt(X*X+Y*Y);
                double GR=mass*constants::MGM_sun/std::sqrt(X*X+Y*Y)*constants::auyr_to_kms/constants::c;
                double SR=0.5*(VX*VX+VY*VY)*constants::auyr_to_kms/constants::c;
                
                for (auto && mapEl : dX )
                {
                  drv[mapEl.first]+=relRedshift*( (VX*dVx[mapEl.first]+VY*dVy[mapEl.first])*constants::auyr_to_kms/constants::c - GR*(X*dX[mapEl.first]+Y*dY[mapEl.first])/r/r);
                  
                }
                drv["Mass"]+=GR/mass;
                drv["Redshift"]+=SR+GR;
                
                return relRedshift*(SR+GR);
              }          
                        
        };
        
 

        LOAD_ORBITS("keplerRedshift", keplerRedshift)
        
    }
    
}

#endif
